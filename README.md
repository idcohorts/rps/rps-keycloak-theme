# Keycloak Theming
This is a simple example of how to theme keycloak using Jinja2 templates. Complete with a docker-compose file to run keycloak locally.

There are 3 parts:
- a docker container running a local keycloak server 
- a folder (rps-keycloak-theme) with the keycloak template (FTL - https://freemarker.apache.org/)
- a docker build service that templates with the config.yaml some jinja files into the keycloak theme

### References:
- https://medium.com/front-end-weekly/custom-themes-for-keycloak-631bdd3e04e5
- https://www.baeldung.com/spring-keycloak-custom-themes

# Usage

Initial setup
```bash
docker compose --profile full up
```

Just Building the Jinja2 files
```bash
docker compose --profile build up
```

Just restart keycloak
```bash
docker compose --profile keycloak-dev up
```

Go to 127.0.0.1:5009/realm/rps
