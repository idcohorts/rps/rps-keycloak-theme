<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>
    <style>
.visible.invisible {
    visibility: hidden;
    max-height: 0;
    opacity: 0;
    transition: max-height 0.5s ease-in, opacity 0.5s ease-in;
}
.back-to-login {
    margin: 53px auto 0 auto;
    text-align: center;
}

.back-to-login a {
    color: var(--global--color-dark-gray) !important;
    font-size: 14px;
}

.pf-c-button.pf-m-primary {
    color: var(--pf-c-button--m-primary--Color);
    background-color: var(--pf-c-button--m-primary--BackgroundColor);
    
    background: var(--global--color-background, --pf-c-button--m-primary--BackgroundColor) !important;
    color: var(--global--color-dark-gray, --pf-c-button--m-primary--Color) !important;
    border-style: solid;
    border-width: 2px;
    font-weight: 700;
    border-color: var(--global--color-dark-gray, --pf-c-button--m-primary--Color) !important;
    border-radius: 5px;
    box-shadow: 0px 0px 5px 0px rgb(255 255 255 / 50%);
    text-decoration: none;
  }

.pf-c-button.pf-m-primary:hover {
    background: var(--global--color-dark-gray, --pf-c-button--m-primary--BackgroundColor) !important;
    color: var(--global--color-background, --pf-c-button--m-primary--Color) !important;
}

.visible {
    visibility: visible;
    opacity: 1;
    transition: max-height 0.5s ease-in, opacity 0.5s ease-in;
    max-height: 400em;
}

/* Mark input boxes that gets an error on validation: */
select.invalid,
input.invalid {
    background-color: #ffdddd;
}

.d-none {
    display: none;
}

ul.form-stepper {
    counter-reset: section;
    margin-bottom: 3rem;
}
ul.form-stepper .form-stepper-circle {
    position: relative;
}
ul.form-stepper .form-stepper-circle span {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateY(-50%) translateX(-50%);
}
.form-stepper-horizontal {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
}
ul.form-stepper > li:not(:last-of-type) {
    margin-bottom: 0.625rem;
    -webkit-transition: margin-bottom 0.4s;
    -o-transition: margin-bottom 0.4s;
    transition: margin-bottom 0.4s;
}
.form-stepper-horizontal > li:not(:last-of-type) {
    margin-bottom: 0 !important;
}
.form-stepper-horizontal li {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: start;
    -webkit-transition: 0.5s;
    transition: 0.5s;
}
.form-stepper-horizontal li:not(:last-child):after {
    position: relative;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    height: 1px;
    content: "";
    top: 49%; /* => fixes rendering bug */
}
.form-stepper-horizontal li:after {
    background-color: #dee2e6;
}
.form-stepper-horizontal li.form-stepper-completed:after {
    background-color: var(--global--color-primary, #4361ee) !important;
}
.form-stepper-horizontal li:last-child {
    flex: unset;
}
ul.form-stepper li a .form-stepper-circle {
    display: inline-block;
    width: 40px;
    height: 40px;
    margin-right: 0;
    line-height: 1.7rem;
    text-align: center;
    background: rgba(0, 0, 0, 0.38);
    border-radius: 50%;
}
.form-stepper .form-stepper-active .form-stepper-circle {
    background-color: var(--global--color-primary, #4361ee) !important;
    color: #fff;
}
.form-stepper .form-stepper-active .label {
    color: var(--global--color-primary, #4361ee) !important;
}
.form-stepper .form-stepper-active .form-stepper-circle:hover {
    background-color: var(--global--color-primary, #4361ee) !important;
    color: #fff !important;
}
.form-stepper .form-stepper-unfinished .form-stepper-circle {
    background-color: #f8f7ff;
}
.form-stepper .form-stepper-completed .form-stepper-circle {
    background-color: var(--global--color-secondary, #0e9594) !important;
    color: #fff;
}
.form-stepper .form-stepper-completed .label {
    color: var(--global--color-secondary, #0e9594) !important;
}
.form-stepper .form-stepper-completed .form-stepper-circle:hover {
    background-color: var(--global--color-secondary, #0e9594) !important;
    color: #fff !important;
}
.form-stepper .form-stepper-active span.text-muted {
    color: #fff !important;
}
.form-stepper .form-stepper-completed span.text-muted {
    color: #fff !important;
}
.form-stepper .label {
    font-size: 1rem;
    margin-top: 0.5rem;
}
.form-stepper a {
    cursor: default;
}</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script> 
        const rules = [{"id": "show-partner-organisation", "rules": [{"id": "organisation_type", "value": "Nicht-universit\u00e4re Partnereinrichtung"}], "show": {"form_item": "Organisation", "options": ["num-organisations--cottbus", "num-organisations--dkfz", "num-organisations--fraunhofer-mevis", "num-organisations--highmed", "num-organisations--hmgu", "num-organisations--hzi", "num-organisations--ths-heilbronn", "num-organisations--tmf", "num-organisations--tu-darmstadt"]}, "triggered_by": "organisation_type"}, {"id": "show-uniklinik-organisation", "rules": [{"id": "organisation_type", "value": "Uniklinik"}], "show": {"form_item": "Organisation", "options": ["num-organisations--uk-augsburg", "num-organisations--rwth-aachen", "num-organisations--uk-augsburg", "num-organisations--charite-berlin", "num-organisations--uk-owl", "num-organisations--uk-bochum", "num-organisations--uk-bonn", "num-organisations--uk-dresden", "num-organisations--uk-duesseldorf", "num-organisations--uk-erlangen", "num-organisations--uk-essen", "num-organisations--uk-frankfurt", "num-organisations--uk-freiburg", "num-organisations--uk-marburg", "num-organisations--uk-goettingen", "num-organisations--uk-greifswald", "num-organisations--uk-halle", "num-organisations--uk-hamburg-eppendorf", "num-organisations--mh-hannover", "num-organisations--uk-heidelberg", "num-organisations--uk-saarland", "num-organisations--uk-jena", "num-organisations--uk-koeln", "num-organisations--uk-leipzig", "num-organisations--uk-magdeburg", "num-organisations--uk-mainz", "num-organisations--uk-mannheim", "num-organisations--lmu-muenchen", "num-organisations--k-recht-der-isar-muenchen", "num-organisations--uk-muenster", "num-organisations--uk-oldenburg", "num-organisations--uk-regensburg", "num-organisations--uk-rostock", "num-organisations--uk-schleswig-holstein", "num-organisations--uk-tuebingen", "num-organisations--uk-ulm", "num-organisations--uk-wuerzburg"]}, "triggered_by": "organisation_type"}, {"id": "show-num-functions", "rules": [{"id": "Ober-Organisation", "value": "unikliniken"}], "show": {"form_item": "NUM-Funktion", "options": ["loks-l", "loks-m"]}, "triggered_by": "Ober-Organisation"}, {"id": "show-num-functions", "rules": [{"id": "Ober-Organisation", "value": "otherpartners"}], "show": {"form_item": "NUM-Funktion", "options": null}, "triggered_by": "Ober-Organisation"}, {"id": "show-forschungslinie-projekte", "rules": [{"id": "num-projects-project-type--forschungslinie", "value": true}], "show": {"form_item": "Forschungslinie", "options": null}, "triggered_by": "num-projects-project-type--forschungslinie"}, {"id": "show-infrastrukturlinie-projekte", "rules": [{"id": "num-projects-project-type--infrastrukturlinie", "value": true}], "show": {"form_item": "Infrastrukturlinie", "options": null}, "triggered_by": "num-projects-project-type--infrastrukturlinie"}, {"id": "show-fosa-liste", "rules": [{"id": "fosa-beteiligung", "value": "ja-fosa-beteiligung"}], "show": {"form_item": "Fachbereich", "options": null}, "triggered_by": "fosa-beteiligung"}];      
    </script>
    <script>
function set_user_name() {
    let vorname = document.getElementById("firstName").value;
    let nachname = document.getElementById("lastName").value;
    if (vorname && vorname != '' && nachname && nachname != '')
    {
      let username = vorname.toLowerCase() + '.' + nachname.toLowerCase();
      let clean_username = username.replace(/[^\w\.]/gi, '')

      console.log(clean_username);
      document.getElementById("username").value = clean_username;
    }
}

function apply_rules(question){

  // find all corresponding rules
  let triggered_rules = rules.filter(rule => ('user.attributes.' + rule.triggered_by) == question.id);

  // check the rules whether trigger should be triggered
  for(let x in triggered_rules) {
    let rule = triggered_rules[x];

    rule.is_triggered = true;
    
    for(let y in rule.rules) {
      let trigger_rule = rule.rules[y];
      let id = 'user.attributes.' + trigger_rule.id;
      let element = document.getElementById(id);
      let value = element.value;

      if (element.type == "checkbox") {
        value = element.checked;
      }

      if (value != trigger_rule.value) {
        rule.is_triggered = false;
      }
    }
  }

  // reset every not triggered rule
  let reset_triggered_rules = triggered_rules.filter(rule => !rule.is_triggered);
  for (let x in reset_triggered_rules) {
    set_questions(reset_triggered_rules[x]);
  }

  // set every triggered rule
  let set_triggered_rules = triggered_rules.filter(rule => rule.is_triggered);
  for (let x in set_triggered_rules) {
    set_questions(set_triggered_rules[x]);
  }        
}

function set_questions(rule) {
  console.log(rule.is_triggered);
  
  let show_id = 'user.attributes.' + rule.show.form_item + ".div";
  console.log(show_id);        
  document.getElementById(show_id).classList.add("invisible");

  if(rule.is_triggered) 
    document.getElementById(show_id).classList.remove("invisible");

  if(rule.show.options) {
    for (let z in rule.show.options) {
      let option = rule.show.options[z]
      let option_id = 'user.attributes.' + rule.show.form_item + "." + option;

      document.getElementById(option_id).classList.add("hidden");
      if(rule.is_triggered) 
      
        document.getElementById(option_id).classList.remove("hidden");
    }
  }
}

function set_other(field, input, html_class) {
  console.log(input.value);
  if ( input.value == 'other' ) {
    let outerHTML = "<input type='text' " + 
      ((field.required == "True") ? 'required ' : '') + 
      "class='" + html_class + "' id='user.attributes." + 
      field.attribute + "' name='user.attributes." + 
      field.attribute + "'  placeholder='"+
      field.other + "' />";
    input.outerHTML = outerHTML;
  }
  else {
    let id = '#' + input.value;
    $(id).removeClass("hidden");
  }      
}

var currentTab = 1; // Current tab is set to be the first tab (0)

function validateForm() {
  // This function deals with validation of the form fields
  var section, inputs, i, valid = true;
  section = document.getElementsByClassName("form-step");
  inputs = section[currentTab -1].querySelectorAll("[required]");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < inputs.length; i++) {
    // If a field is empty...
    if (inputs[i].value == "") {
      // add an "invalid" class to the field:
      inputs[i].classList.add("invalid")
      // and set the current valid status to false:
      valid = false;
    }
    else {
      inputs[i].classList.remove("invalid")
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementById("step-header-"+ currentTab).classList.add("finish");
  }
  return valid; // return the valid status
}

/**
 * Define a function to navigate betweens form steps.
 * It accepts one parameter. That is - step number.
 */
 const navigateToFormStep = (step) => {
    /**
     * Hide all form steps.
     */
    if (step == 1 && !validateForm ()) { 
      return false;
    }

    currentTab += step;
    
    console.log(currentTab)

    if(currentTab >1){
      var headerText = document.getElementsByClassName("alert-info-header");
      for (var i = 0; i < headerText.length; i++) {
        headerText[i].style.display='none';  
      }
      var registerCard = document.getElementsByClassName("card-pf");
      registerCard[0].style["margin-top"]='50px';  
    }
   
    document.querySelectorAll(".form-step").forEach((formStepElement) => {
        formStepElement.classList.add("d-none");
    });
    /**
     * Mark all form steps as unfinished.
     */
    document.querySelectorAll(".form-stepper-list").forEach((formStepHeader) => {
        formStepHeader.classList.add("form-stepper-unfinished");
        formStepHeader.classList.remove("form-stepper-active", "form-stepper-completed");
    });
    /**
     * Show the current form step (as passed to the function).
     */
    document.querySelector("#step-" + currentTab).classList.remove("d-none");
    /**
     * Select the form step circle (progress bar).
     */
    const formStepCircle = document.querySelector('li[step="' + currentTab + '"]');
    /**
     * Mark the current form step as active.
     */
    formStepCircle.classList.remove("form-stepper-unfinished", "form-stepper-completed");
    formStepCircle.classList.add("form-stepper-active");
    /**
     * Loop through each form step circles.
     * This loop will continue up to the current step number.
     * Example: If the current step is 3,
     * then the loop will perform operations for step 1 and 2.
     */
    for (let index = 0; index < currentTab; index++) {
        /**
         * Select the form step circle (progress bar).
         */
        const formStepCircle = document.querySelector('li[step="' + index + '"]');
        /**
         * Check if the element exist. If yes, then proceed.
         */
        if (formStepCircle) {
            /**
             * Mark the form step as completed.
             */
            formStepCircle.classList.remove("form-stepper-unfinished", "form-stepper-active");
            formStepCircle.classList.add("form-stepper-completed");
        }
    }
};</script>

    <div class="row">
        <div class="col-md-10">
            <h2>${msg("editAccountHtmlTitle")}</h2>
        </div>
        <div class="col-md-2 subtitle">
            <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
        </div>
    </div>
    <div class="custom-notification-box">
        Bitte beachten Sie, dass Aktualisierungen Ihres Profils aktuell bis zu 24h benötigen können, um über alle Bereiche des NUM-Hubs synchronisiert zu werden. Weitern können Sie Ihr Profil bereits vor der Aktivierung durch die Koordinierungsstelle einsehen. Sobald Ihr Account aktiviert wurde, können Sie den <a href="https://support.netzwerk-universitaetsmedizin.de/Antragsprozess.html">NUM-Hub betreten</a>.
    </div>
    <form action="${url.accountUrl}" class="form-horizontal" method="post">
     
     
          <h2>Persönliche Angaben</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div  class="form-group
       "
      id="user.attributes.form_of_address.div" >
    <div class="col-sm-2 col-md-2">
      Anrede
      
        <sub>*</sub>
      
      
    </div>

    <div class="col-sm-10 col-md-10">
      <select  required 
          class="${properties.kcInputClass!}" id="user.attributes.form_of_address" 
          name="user.attributes.form_of_address" 
          value="account.form_of_address" 
          onchange='apply_rules(this); set_other({"attribute": "form_of_address", "default": "W\u00e4hlen Sie eine von Ihnen gew\u00fcnschte Anrede aus", "description": "Wir nutzen die Anrede, um Sie in unseren E-Mails korrekt anzusprechen.", "options": [{"description": "Sehr geehrte Frau", "value": "Sehr geehrte Frau"}, {"description": "Sehr geehrter Herr", "value": "Sehr geehrter Herr"}, {"description": "Sehr geehrte*", "value": "Sehr geehrte*"}, {"description": "Guten Tag", "value": "Guten Tag"}], "required": true, "title": "Anrede", "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
        
          <option selected disabled hidden style='display: none' value=''>Wählen Sie eine von Ihnen gewünschte Anrede aus</option>
        
        
          <option id="user.attributes.form_of_address.Sehr geehrte Frau" 
                  value="Sehr geehrte Frau"                
                  <#if account.attributes["form_of_address"]??>
                  <#if account.attributes["form_of_address"] == "Sehr geehrte Frau" > selected </#if>
                  </#if>
                  class="">                
            Sehr geehrte Frau
          </option>
        
          <option id="user.attributes.form_of_address.Sehr geehrter Herr" 
                  value="Sehr geehrter Herr"                
                  <#if account.attributes["form_of_address"]??>
                  <#if account.attributes["form_of_address"] == "Sehr geehrter Herr" > selected </#if>
                  </#if>
                  class="">                
            Sehr geehrter Herr
          </option>
        
          <option id="user.attributes.form_of_address.Sehr geehrte*" 
                  value="Sehr geehrte*"                
                  <#if account.attributes["form_of_address"]??>
                  <#if account.attributes["form_of_address"] == "Sehr geehrte*" > selected </#if>
                  </#if>
                  class="">                
            Sehr geehrte*
          </option>
        
          <option id="user.attributes.form_of_address.Guten Tag" 
                  value="Guten Tag"                
                  <#if account.attributes["form_of_address"]??>
                  <#if account.attributes["form_of_address"] == "Guten Tag" > selected </#if>
                  </#if>
                  class="">                
            Guten Tag
          </option>
        
        
      </select>
    </div>
</div>

          
              <div class="form-group">
    <div class="col-sm-2 col-md-2">
      Titel
      
    </div>
    <div class="col-sm-10 col-md-10">
      
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.title" 
        name="user.attributes.title" 
        value="${(account.attributes.title!'')}"  
        placeholder="z.B. Dr., Prof., PD"/>
   </div>
</div>
          
              <div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">
    <div class="col-sm-2 col-md-2">
        <label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">*</span>
    </div>

    <div class="col-sm-10 col-md-10">
        <input type="text" class="form-control" id="firstName" name="firstName" value="${(account.firstName!'')}"/>
    </div>
</div>
          
              <div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">
    <div class="col-sm-2 col-md-2">
        <label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">*</span>
    </div>

    <div class="col-sm-10 col-md-10">
        <input type="text" class="form-control" id="lastName" name="lastName" value="${(account.lastName!'')}"/>
    </div>
</div>
          
              <div class="form-group ${messagesPerField.printIfExists('email','has-error')}">
    <div class="col-sm-2 col-md-2">
        <label for="email" class="control-label">${msg("email")}</label> <span class="required">*</span>
    </div>

    <div class="col-sm-10 col-md-10">
        <input type="text" class="form-control" id="email" name="email" readonly="readonly" autofocus value="${(account.email!'')}"/>
    </div>
</div>
          
              <div class="form-group">
    <div class="col-sm-2 col-md-2">
      Telefonnummer (Beruflich)
      
    </div>
    <div class="col-sm-10 col-md-10">
      
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.phone" 
        name="user.attributes.phone" 
        value="${(account.attributes.phone!'')}"  
        />
   </div>
</div>
            
          
      
     
          <h2>Berufliche Angaben</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div  class="form-group
       "
      id="user.attributes.organisation_type.div" >
    <div class="col-sm-2 col-md-2">
      Organisation
      
        <sub>*</sub>
      
      
    </div>

    <div class="col-sm-10 col-md-10">
      <select  required 
          class="${properties.kcInputClass!}" id="user.attributes.organisation_type" 
          name="user.attributes.organisation_type" 
          value="account.organisation_type" 
          onchange='apply_rules(this); set_other({"attribute": "organisation_type", "default": "W\u00e4hlen Sie Ihre Organisation aus.", "options": [{"description": "Uniklinik", "value": "Uniklinik"}, {"description": "Nicht-universit\u00e4re Partnereinrichtung", "value": "Nicht-universit\u00e4re Partnereinrichtung"}], "required": true, "title": "Organisation", "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
        
          <option selected disabled hidden style='display: none' value=''>Wählen Sie Ihre Organisation aus.</option>
        
        
          <option id="user.attributes.organisation_type.Uniklinik" 
                  value="Uniklinik"                
                  <#if account.attributes["organisation_type"]??>
                  <#if account.attributes["organisation_type"] == "Uniklinik" > selected </#if>
                  </#if>
                  class="">                
            Uniklinik
          </option>
        
          <option id="user.attributes.organisation_type.Nicht-universitäre Partnereinrichtung" 
                  value="Nicht-universitäre Partnereinrichtung"                
                  <#if account.attributes["organisation_type"]??>
                  <#if account.attributes["organisation_type"] == "Nicht-universitäre Partnereinrichtung" > selected </#if>
                  </#if>
                  class="">                
            Nicht-universitäre Partnereinrichtung
          </option>
        
        
      </select>
    </div>
</div>

          
              <div  class="form-group
        visible invisible "
      id="user.attributes.Organisation.div" >
    <div class="col-sm-2 col-md-2">
      None
      
      
    </div>

    <div class="col-sm-10 col-md-10">
      <select  required 
          class="${properties.kcInputClass!}" id="user.attributes.Organisation" 
          name="user.attributes.Organisation" 
          value="account.Organisation" 
          onchange='apply_rules(this); set_other({"attribute": "Organisation", "default": "W\u00e4hlen Sie Ihre Organisation aus.", "hidden": true, "options": [{"description": "Uniklinik RWTH Aachen", "value": "num-organisations--rwth-aachen"}, {"description": "Universit\u00e4tsklinikum Augsburg", "value": "num-organisations--uk-augsburg"}, {"description": "Charit\u00e9 - Universit\u00e4tsmedizin Berlin", "value": "num-organisations--charite-berlin"}, {"description": "Universit\u00e4tsklinikum OWL", "value": "num-organisations--uk-owl"}, {"description": "Universit\u00e4tsklinikum der Ruhr-Universit\u00e4t Bochum", "value": "num-organisations--uk-bochum"}, {"description": "Universit\u00e4tsklinikum Bonn", "value": "num-organisations--uk-bonn"}, {"description": "Universit\u00e4tsklinikum Carl Gustav Carus Dresden", "value": "num-organisations--uk-dresden"}, {"description": "Universit\u00e4tsklinikum D\u00fcsseldorf", "value": "num-organisations--uk-duesseldorf"}, {"description": "Universit\u00e4tsklinikum Erlangen", "value": "num-organisations--uk-erlangen"}, {"description": "Universit\u00e4tsklinikum Essen", "value": "num-organisations--uk-essen"}, {"description": "Universit\u00e4tsklinikum Frankfurt", "value": "num-organisations--uk-frankfurt"}, {"description": "Universit\u00e4tsklinikum Freiburg", "value": "num-organisations--uk-freiburg"}, {"description": "Uniklinikum Gie\u00dfen und Marburg", "value": "num-organisations--uk-marburg"}, {"description": "Universit\u00e4tsmedizin G\u00f6ttingen", "value": "num-organisations--uk-goettingen"}, {"description": "Universit\u00e4tsmedizin Greifswald", "value": "num-organisations--uk-greifswald"}, {"description": "Universit\u00e4tsklinikum Halle (Saale)", "value": "num-organisations--uk-halle"}, {"description": "Universit\u00e4tsklinikum Hamburg-Eppendorf", "value": "num-organisations--uk-hamburg-eppendorf"}, {"description": "Medizinische Hochschule Hannover", "value": "num-organisations--mh-hannover"}, {"description": "Universit\u00e4tsklinikum Heidelberg", "value": "num-organisations--uk-heidelberg"}, {"description": "Universit\u00e4tsklinikum des Saarlandes", "value": "num-organisations--uk-saarland"}, {"description": "Universit\u00e4tsklinikum Jena", "value": "num-organisations--uk-jena"}, {"description": "Uniklinik K\u00f6ln", "value": "num-organisations--uk-koeln"}, {"description": "Universit\u00e4tsklinikum Leipzig", "value": "num-organisations--uk-leipzig"}, {"description": "Universit\u00e4tsmedizin Magdeburg", "value": "num-organisations--uk-magdeburg"}, {"description": "Universit\u00e4tsmedizin Mainz", "value": "num-organisations--uk-mainz"}, {"description": "Universit\u00e4tsmedizin Mannheim", "value": "num-organisations--uk-mannheim"}, {"description": "LMU Klinikum M\u00fcnchen", "value": "num-organisations--lmu-muenchen"}, {"description": "Klinikum rechts der Isar M\u00fcnchen", "value": "num-organisations--k-recht-der-isar-muenchen"}, {"description": "Universit\u00e4tsklinikum M\u00fcnster", "value": "num-organisations--uk-muenster"}, {"description": "Universit\u00e4tsmedizin Oldenburg", "value": "num-organisations--uk-oldenburg"}, {"description": "Universit\u00e4tsklinikum Regensburg", "value": "num-organisations--uk-regensburg"}, {"description": "Universit\u00e4tsmedizin Rostock", "value": "num-organisations--uk-rostock"}, {"description": "Universit\u00e4tsklinikum Schleswig-Holstein", "value": "num-organisations--uk-schleswig-holstein"}, {"description": "Universit\u00e4tsklinikum T\u00fcbingen", "value": "num-organisations--uk-tuebingen"}, {"description": "Universit\u00e4tsklinikum Ulm", "value": "num-organisations--uk-ulm"}, {"description": "Universit\u00e4tsklinikum W\u00fcrzburg", "value": "num-organisations--uk-wuerzburg"}, {"description": "Carl-Thiem Klinikum Cottbus", "value": "num-organisations--cottbus"}, {"description": "DKFZ", "value": "num-organisations--dkfz"}, {"description": "Fraunhofer MEVIS", "value": "num-organisations--fraunhofer-mevis"}, {"description": "HIGHMED", "value": "num-organisations--highmed"}, {"description": "Helmholtz-Zentrum M\u00fcnchen (HMGU)", "value": "num-organisations--hmgu"}, {"description": "Helmholtz-Zentrum f\u00fcr Infektionsforschung (HZI)", "value": "num-organisations--hzi"}, {"description": "Technische Hochschule Heilbronn", "value": "num-organisations--ths-heilbronn"}, {"description": "TMF", "value": "num-organisations--tmf"}, {"description": "TU Darmstadt", "value": "num-organisations--tu-darmstadt"}, {"description": "Liste wird kommen", "value": "num-organisations--other"}], "required": true, "title": null, "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
        
          <option selected disabled hidden style='display: none' value=''>Wählen Sie Ihre Organisation aus.</option>
        
        
          <option id="user.attributes.Organisation.num-organisations--rwth-aachen" 
                  value="num-organisations--rwth-aachen"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--rwth-aachen" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Uniklinik RWTH Aachen
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-augsburg" 
                  value="num-organisations--uk-augsburg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-augsburg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Augsburg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--charite-berlin" 
                  value="num-organisations--charite-berlin"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--charite-berlin" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Charité - Universitätsmedizin Berlin
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-owl" 
                  value="num-organisations--uk-owl"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-owl" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum OWL
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-bochum" 
                  value="num-organisations--uk-bochum"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-bochum" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum der Ruhr-Universität Bochum
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-bonn" 
                  value="num-organisations--uk-bonn"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-bonn" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Bonn
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-dresden" 
                  value="num-organisations--uk-dresden"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-dresden" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Carl Gustav Carus Dresden
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-duesseldorf" 
                  value="num-organisations--uk-duesseldorf"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-duesseldorf" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Düsseldorf
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-erlangen" 
                  value="num-organisations--uk-erlangen"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-erlangen" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Erlangen
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-essen" 
                  value="num-organisations--uk-essen"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-essen" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Essen
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-frankfurt" 
                  value="num-organisations--uk-frankfurt"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-frankfurt" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Frankfurt
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-freiburg" 
                  value="num-organisations--uk-freiburg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-freiburg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Freiburg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-marburg" 
                  value="num-organisations--uk-marburg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-marburg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Uniklinikum Gießen und Marburg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-goettingen" 
                  value="num-organisations--uk-goettingen"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-goettingen" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsmedizin Göttingen
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-greifswald" 
                  value="num-organisations--uk-greifswald"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-greifswald" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsmedizin Greifswald
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-halle" 
                  value="num-organisations--uk-halle"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-halle" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Halle (Saale)
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-hamburg-eppendorf" 
                  value="num-organisations--uk-hamburg-eppendorf"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-hamburg-eppendorf" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Hamburg-Eppendorf
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--mh-hannover" 
                  value="num-organisations--mh-hannover"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--mh-hannover" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Medizinische Hochschule Hannover
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-heidelberg" 
                  value="num-organisations--uk-heidelberg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-heidelberg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Heidelberg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-saarland" 
                  value="num-organisations--uk-saarland"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-saarland" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum des Saarlandes
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-jena" 
                  value="num-organisations--uk-jena"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-jena" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Jena
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-koeln" 
                  value="num-organisations--uk-koeln"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-koeln" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Uniklinik Köln
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-leipzig" 
                  value="num-organisations--uk-leipzig"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-leipzig" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Leipzig
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-magdeburg" 
                  value="num-organisations--uk-magdeburg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-magdeburg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsmedizin Magdeburg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-mainz" 
                  value="num-organisations--uk-mainz"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-mainz" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsmedizin Mainz
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-mannheim" 
                  value="num-organisations--uk-mannheim"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-mannheim" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsmedizin Mannheim
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--lmu-muenchen" 
                  value="num-organisations--lmu-muenchen"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--lmu-muenchen" > selected </#if>
                  </#if>
                  class=" hidden ">                
            LMU Klinikum München
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--k-recht-der-isar-muenchen" 
                  value="num-organisations--k-recht-der-isar-muenchen"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--k-recht-der-isar-muenchen" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Klinikum rechts der Isar München
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-muenster" 
                  value="num-organisations--uk-muenster"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-muenster" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Münster
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-oldenburg" 
                  value="num-organisations--uk-oldenburg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-oldenburg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsmedizin Oldenburg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-regensburg" 
                  value="num-organisations--uk-regensburg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-regensburg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Regensburg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-rostock" 
                  value="num-organisations--uk-rostock"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-rostock" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsmedizin Rostock
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-schleswig-holstein" 
                  value="num-organisations--uk-schleswig-holstein"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-schleswig-holstein" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Schleswig-Holstein
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-tuebingen" 
                  value="num-organisations--uk-tuebingen"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-tuebingen" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Tübingen
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-ulm" 
                  value="num-organisations--uk-ulm"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-ulm" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Ulm
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--uk-wuerzburg" 
                  value="num-organisations--uk-wuerzburg"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--uk-wuerzburg" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Universitätsklinikum Würzburg
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--cottbus" 
                  value="num-organisations--cottbus"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--cottbus" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Carl-Thiem Klinikum Cottbus
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--dkfz" 
                  value="num-organisations--dkfz"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--dkfz" > selected </#if>
                  </#if>
                  class=" hidden ">                
            DKFZ
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--fraunhofer-mevis" 
                  value="num-organisations--fraunhofer-mevis"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--fraunhofer-mevis" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Fraunhofer MEVIS
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--highmed" 
                  value="num-organisations--highmed"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--highmed" > selected </#if>
                  </#if>
                  class=" hidden ">                
            HIGHMED
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--hmgu" 
                  value="num-organisations--hmgu"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--hmgu" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Helmholtz-Zentrum München (HMGU)
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--hzi" 
                  value="num-organisations--hzi"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--hzi" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Helmholtz-Zentrum für Infektionsforschung (HZI)
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--ths-heilbronn" 
                  value="num-organisations--ths-heilbronn"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--ths-heilbronn" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Technische Hochschule Heilbronn
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--tmf" 
                  value="num-organisations--tmf"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--tmf" > selected </#if>
                  </#if>
                  class=" hidden ">                
            TMF
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--tu-darmstadt" 
                  value="num-organisations--tu-darmstadt"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--tu-darmstadt" > selected </#if>
                  </#if>
                  class=" hidden ">                
            TU Darmstadt
          </option>
        
          <option id="user.attributes.Organisation.num-organisations--other" 
                  value="num-organisations--other"                
                  <#if account.attributes["Organisation"]??>
                  <#if account.attributes["Organisation"] == "num-organisations--other" > selected </#if>
                  </#if>
                  class=" hidden ">                
            Liste wird kommen
          </option>
        
        
      </select>
    </div>
</div>

          
              <div class="form-group">
    <div class="col-sm-2 col-md-2">
      Bereich <sub>*</sub>
      
    </div>
    <div class="col-sm-10 col-md-10">
      
       <input type="text" required 
        class="${properties.kcInputClass!}" 
        id="user.attributes.department" 
        name="user.attributes.department" 
        value="${(account.attributes.department!'')}"  
        placeholder="z.B. Abteilung, Institut, Geschäftsstelle, Referat, Klinik"/>
   </div>
</div>
          
              <div class="form-group">
    <div class="col-sm-2 col-md-2">
      Funktion
      
    </div>
    <div class="col-sm-10 col-md-10">
      
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.function" 
        name="user.attributes.function" 
        value="${(account.attributes.function!'')}"  
        placeholder="z.B. Leiter Arbeitsgruppe, Assistenzarzt, Referent, Mitarbeiter Drittmittelverwaltung, Study Nurse"/>
   </div>
</div>
            
          
      
     
          <h2>Angaben in Bezug zum Netzwerk Universitätsmedizin</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div class="form-group">
  <div class="col-sm-2 col-md-2">
    <div id="user.attributes.NUM-Funktion.div"  
          class="${properties.kcInputWrapperClass!} ">
          Primäre Rolle im Netzwerk Universitätsmedizin
            
      </div>
  </div>    
  <div class="col-sm-10 col-md-10">
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--loks-l" 
              id="user.attributes.num-roles--loks-l" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--loks-l"]??>
              <#if account.attributes["num-roles--loks-l"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--loks-l">Leiter*in Lokale Stabsstelle</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--loks-m" 
              id="user.attributes.num-roles--loks-m" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--loks-m"]??>
              <#if account.attributes["num-roles--loks-m"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--loks-m">Mitarbeiter*in Lokale Stabsstelle</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--dmv" 
              id="user.attributes.num-roles--dmv" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--dmv"]??>
              <#if account.attributes["num-roles--dmv"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--dmv">Team Drittmittelverwaltung</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--tp-l" 
              id="user.attributes.num-roles--tp-l" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--tp-l"]??>
              <#if account.attributes["num-roles--tp-l"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--tp-l">Teilprojektleiter*in</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--tp-m" 
              id="user.attributes.num-roles--tp-m" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--tp-m"]??>
              <#if account.attributes["num-roles--tp-m"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--tp-m">Teilprojektmitarbeiter*in</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--ks" 
              id="user.attributes.num-roles--ks" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--ks"]??>
              <#if account.attributes["num-roles--ks"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--ks">Koordinierungsstelle</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--antrs" 
              id="user.attributes.num-roles--antrs" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--antrs"]??>
              <#if account.attributes["num-roles--antrs"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--antrs">NUM Antragsteller*in</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--kp" 
              id="user.attributes.num-roles--kp" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--kp"]??>
              <#if account.attributes["num-roles--kp"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--kp">Kooperationspartner*in in einem NUM-Projekt</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--uac" 
              id="user.attributes.num-roles--uac" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--uac"]??>
              <#if account.attributes["num-roles--uac"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--uac">Antragsteller*in Use and Access</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-roles--gremien" 
              id="user.attributes.num-roles--gremien" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-roles--gremien"]??>
              <#if account.attributes["num-roles--gremien"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-roles--gremien">Miarbeiter*in Gremienportal</label>
      </div>
    
  </div>
</div>


          
              <div class="form-group">
  <div class="col-sm-2 col-md-2">
    <div id="user.attributes.Projektteilnahme.div"  
          class="${properties.kcInputWrapperClass!} ">
          Beteiligung an den NUM-Projekten
            
      </div>
  </div>    
  <div class="col-sm-10 col-md-10">
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects-project-type--infrastrukturlinie" 
              id="user.attributes.num-projects-project-type--infrastrukturlinie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects-project-type--infrastrukturlinie"]??>
              <#if account.attributes["num-projects-project-type--infrastrukturlinie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects-project-type--infrastrukturlinie">Infrastrukturlinie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects-project-type--forschungslinie" 
              id="user.attributes.num-projects-project-type--forschungslinie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects-project-type--forschungslinie"]??>
              <#if account.attributes["num-projects-project-type--forschungslinie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects-project-type--forschungslinie">Forschungslinie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--none" 
              id="user.attributes.num-projects--none" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--none"]??>
              <#if account.attributes["num-projects--none"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--none">Keine Beteiligung</label>
      </div>
    
  </div>
</div>


          
              <div class="form-group">
  <div class="col-sm-2 col-md-2">
    <div id="user.attributes.Infrastrukturlinie.div"  
          class="${properties.kcInputWrapperClass!}  visible invisible ">
          Beteiligung an den NUM-Projekten der Infrastrukturlinie
            
      </div>
  </div>    
  <div class="col-sm-10 col-md-10">
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--atkin" 
              id="user.attributes.num-projects--atkin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--atkin"]??>
              <#if account.attributes["num-projects--atkin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--atkin">AKTIN</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--gensurv" 
              id="user.attributes.num-projects--gensurv" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--gensurv"]??>
              <#if account.attributes["num-projects--gensurv"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--gensurv">GenSurv</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--nukleus" 
              id="user.attributes.num-projects--nukleus" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--nukleus"]??>
              <#if account.attributes["num-projects--nukleus"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--nukleus">NUKLEUS</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--num-rdp" 
              id="user.attributes.num-projects--num-rdp" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--num-rdp"]??>
              <#if account.attributes["num-projects--num-rdp"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--num-rdp">NUM-RDP</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--racoon" 
              id="user.attributes.num-projects--racoon" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--racoon"]??>
              <#if account.attributes["num-projects--racoon"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--racoon">RACOON</label>
      </div>
    
  </div>
</div>


          
              <div class="form-group">
  <div class="col-sm-2 col-md-2">
    <div id="user.attributes.Forschungslinie.div"  
          class="${properties.kcInputWrapperClass!}  visible invisible ">
          Beteiligung an den NUM-Projekten der Forschungslinie
            
      </div>
  </div>    
  <div class="col-sm-10 col-md-10">
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--aktin-2-0" 
              id="user.attributes.num-projects--aktin-2-0" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--aktin-2-0"]??>
              <#if account.attributes["num-projects--aktin-2-0"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--aktin-2-0">AKTIN 2.0</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--codexplus" 
              id="user.attributes.num-projects--codexplus" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--codexplus"]??>
              <#if account.attributes["num-projects--codexplus"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--codexplus">CODEX+</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--collpan" 
              id="user.attributes.num-projects--collpan" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--collpan"]??>
              <#if account.attributes["num-projects--collpan"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--collpan">CollPan</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--coverchild" 
              id="user.attributes.num-projects--coverchild" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--coverchild"]??>
              <#if account.attributes["num-projects--coverchild"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--coverchild">COVerCHILD</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--covim" 
              id="user.attributes.num-projects--covim" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--covim"]??>
              <#if account.attributes["num-projects--covim"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--covim">COVIM</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--moltrax" 
              id="user.attributes.num-projects--moltrax" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--moltrax"]??>
              <#if account.attributes["num-projects--moltrax"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--moltrax">MolTrax</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--napkon-tip" 
              id="user.attributes.num-projects--napkon-tip" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--napkon-tip"]??>
              <#if account.attributes["num-projects--napkon-tip"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--napkon-tip">NAPKON TIP</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--napkon" 
              id="user.attributes.num-projects--napkon" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--napkon"]??>
              <#if account.attributes["num-projects--napkon"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--napkon">NAPKON</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--prepared" 
              id="user.attributes.num-projects--prepared" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--prepared"]??>
              <#if account.attributes["num-projects--prepared"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--prepared">PREPARED</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--racoon-combined" 
              id="user.attributes.num-projects--racoon-combined" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--racoon-combined"]??>
              <#if account.attributes["num-projects--racoon-combined"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--racoon-combined">RACOON COMBINE</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-projects--utn-telemedizin" 
              id="user.attributes.num-projects--utn-telemedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-projects--utn-telemedizin"]??>
              <#if account.attributes["num-projects--utn-telemedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-projects--utn-telemedizin">UTN-Telemedizin</label>
      </div>
    
  </div>
</div>


            
          
      
     
          <h2>Angaben in Bezug zum Netzwerk Universitätsmedizin</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div  class="form-group
       "
      id="user.attributes.fosa-beteiligung.div" >
    <div class="col-sm-2 col-md-2">
      Beteiligung an den FOSAs
      
        <sub>*</sub>
      
      
    </div>

    <div class="col-sm-10 col-md-10">
      <select  required 
          class="${properties.kcInputClass!}" id="user.attributes.fosa-beteiligung" 
          name="user.attributes.fosa-beteiligung" 
          value="account.fosa-beteiligung" 
          onchange='apply_rules(this); set_other({"attribute": "fosa-beteiligung", "default": "W\u00e4hlen Sie aus...", "options": [{"description": "Ja", "value": "ja-fosa-beteiligung"}, {"description": "Keine Beteiligung", "value": "none-fosa-beteiligung"}], "required": true, "title": "Beteiligung an den FOSAs", "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
        
          <option selected disabled hidden style='display: none' value=''>Wählen Sie aus...</option>
        
        
          <option id="user.attributes.fosa-beteiligung.ja-fosa-beteiligung" 
                  value="ja-fosa-beteiligung"                
                  <#if account.attributes["fosa-beteiligung"]??>
                  <#if account.attributes["fosa-beteiligung"] == "ja-fosa-beteiligung" > selected </#if>
                  </#if>
                  class="">                
            Ja
          </option>
        
          <option id="user.attributes.fosa-beteiligung.none-fosa-beteiligung" 
                  value="none-fosa-beteiligung"                
                  <#if account.attributes["fosa-beteiligung"]??>
                  <#if account.attributes["fosa-beteiligung"] == "none-fosa-beteiligung" > selected </#if>
                  </#if>
                  class="">                
            Keine Beteiligung
          </option>
        
        
      </select>
    </div>
</div>

          
              <div class="form-group">
  <div class="col-sm-2 col-md-2">
    <div id="user.attributes.Fachbereich.div"  
          class="${properties.kcInputWrapperClass!}  visible invisible ">
          Fachbereich
            
      </div>
  </div>    
  <div class="col-sm-10 col-md-10">
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--allgemeinmedizin" 
              id="user.attributes.num-fosa--allgemeinmedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--allgemeinmedizin"]??>
              <#if account.attributes["num-fosa--allgemeinmedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--allgemeinmedizin">Allgemeinmedizin</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--anaesthesiologie" 
              id="user.attributes.num-fosa--anaesthesiologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--anaesthesiologie"]??>
              <#if account.attributes["num-fosa--anaesthesiologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--anaesthesiologie">Anästhesiologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--dermatologie" 
              id="user.attributes.num-fosa--dermatologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--dermatologie"]??>
              <#if account.attributes["num-fosa--dermatologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--dermatologie">Dermatologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--epidemiologie-public-health" 
              id="user.attributes.num-fosa--epidemiologie-public-health" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--epidemiologie-public-health"]??>
              <#if account.attributes["num-fosa--epidemiologie-public-health"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--epidemiologie-public-health">Epidemiologie & Public Health</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--gastroenterologie" 
              id="user.attributes.num-fosa--gastroenterologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--gastroenterologie"]??>
              <#if account.attributes["num-fosa--gastroenterologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--gastroenterologie">Gastroenterologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--geriatrie" 
              id="user.attributes.num-fosa--geriatrie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--geriatrie"]??>
              <#if account.attributes["num-fosa--geriatrie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--geriatrie">Geriatrie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--gynaekologie-und-geburtshilfe" 
              id="user.attributes.num-fosa--gynaekologie-und-geburtshilfe" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--gynaekologie-und-geburtshilfe"]??>
              <#if account.attributes["num-fosa--gynaekologie-und-geburtshilfe"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--gynaekologie-und-geburtshilfe">Gynäkologie und Geburtshilfe</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--haematologie-onkologie" 
              id="user.attributes.num-fosa--haematologie-onkologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--haematologie-onkologie"]??>
              <#if account.attributes["num-fosa--haematologie-onkologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--haematologie-onkologie">Hämatologie/Onkologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--hno" 
              id="user.attributes.num-fosa--hno" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--hno"]??>
              <#if account.attributes["num-fosa--hno"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--hno">HNO</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--humangenetik" 
              id="user.attributes.num-fosa--humangenetik" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--humangenetik"]??>
              <#if account.attributes["num-fosa--humangenetik"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--humangenetik">Humangenetik</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--immunologie-autoimmunitaet" 
              id="user.attributes.num-fosa--immunologie-autoimmunitaet" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--immunologie-autoimmunitaet"]??>
              <#if account.attributes["num-fosa--immunologie-autoimmunitaet"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--immunologie-autoimmunitaet">Immunologie & Autoimmunität</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--infektologie" 
              id="user.attributes.num-fosa--infektologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--infektologie"]??>
              <#if account.attributes["num-fosa--infektologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--infektologie">Infektologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--intensivmedizin" 
              id="user.attributes.num-fosa--intensivmedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--intensivmedizin"]??>
              <#if account.attributes["num-fosa--intensivmedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--intensivmedizin">Intensivmedizin</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--kardiologie" 
              id="user.attributes.num-fosa--kardiologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--kardiologie"]??>
              <#if account.attributes["num-fosa--kardiologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--kardiologie">Kardiologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--laboratoriumsmedizin" 
              id="user.attributes.num-fosa--laboratoriumsmedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--laboratoriumsmedizin"]??>
              <#if account.attributes["num-fosa--laboratoriumsmedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--laboratoriumsmedizin">Laboratoriumsmedizin</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--mkg-zahnmedizin" 
              id="user.attributes.num-fosa--mkg-zahnmedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--mkg-zahnmedizin"]??>
              <#if account.attributes["num-fosa--mkg-zahnmedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--mkg-zahnmedizin">MKG & Zahnmedizin</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--nephrologie" 
              id="user.attributes.num-fosa--nephrologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--nephrologie"]??>
              <#if account.attributes["num-fosa--nephrologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--nephrologie">Nephrologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--neurologie" 
              id="user.attributes.num-fosa--neurologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--neurologie"]??>
              <#if account.attributes["num-fosa--neurologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--neurologie">Neurologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--neuroradiologie" 
              id="user.attributes.num-fosa--neuroradiologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--neuroradiologie"]??>
              <#if account.attributes["num-fosa--neuroradiologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--neuroradiologie">Neuroradiologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--notfallmedizin" 
              id="user.attributes.num-fosa--notfallmedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--notfallmedizin"]??>
              <#if account.attributes["num-fosa--notfallmedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--notfallmedizin">Notfallmedizin</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--paediatrie" 
              id="user.attributes.num-fosa--paediatrie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--paediatrie"]??>
              <#if account.attributes["num-fosa--paediatrie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--paediatrie">Pädiatrie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--palliativmedizin" 
              id="user.attributes.num-fosa--palliativmedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--palliativmedizin"]??>
              <#if account.attributes["num-fosa--palliativmedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--palliativmedizin">Palliativmedizin</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--pneumologie" 
              id="user.attributes.num-fosa--pneumologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--pneumologie"]??>
              <#if account.attributes["num-fosa--pneumologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--pneumologie">Pneumologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--psychische-gesundheit" 
              id="user.attributes.num-fosa--psychische-gesundheit" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--psychische-gesundheit"]??>
              <#if account.attributes["num-fosa--psychische-gesundheit"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--psychische-gesundheit">Psychische-Gesundheit</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--radiologie" 
              id="user.attributes.num-fosa--radiologie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--radiologie"]??>
              <#if account.attributes["num-fosa--radiologie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--radiologie">Radiologie</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--rehabilitation" 
              id="user.attributes.num-fosa--rehabilitation" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--rehabilitation"]??>
              <#if account.attributes["num-fosa--rehabilitation"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--rehabilitation">Rehabilitation</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--schmerzmedizin" 
              id="user.attributes.num-fosa--schmerzmedizin" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--schmerzmedizin"]??>
              <#if account.attributes["num-fosa--schmerzmedizin"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--schmerzmedizin">Schmerzmedizin</label>
      </div>
    
      <div class="${properties.kcInputWrapperClass!}">
        <input type="checkbox"
              name="user.attributes.num-fosa--strahlentherapie" 
              id="user.attributes.num-fosa--strahlentherapie" 
              onchange="apply_rules(this);"
              <#if account.attributes["num-fosa--strahlentherapie"]??>
              <#if account.attributes["num-fosa--strahlentherapie"] == "on" > checked </#if></#if> />
        <label for="user.attributes.num-fosa--strahlentherapie">Strahlentherapie</label>
      </div>
    
  </div>
</div>


          
              <div class="form-group">
    <div class="col-sm-2 col-md-2">
      ORCID
      
    </div>
    <div class="col-sm-10 col-md-10">
      <p><a href="https://orcid.org" target="_blank" >ORCID</a> wird genutzt, um Informationen zur wissenschaftlichen Arbeit (Publikationen und Poster) der Nutzer:innen abzufragen und diese intern im NUM- Hub anzuzeigen</p>
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.ORCID" 
        name="user.attributes.ORCID" 
        value="${(account.attributes.ORCID!'')}"  
        placeholder="0123-4567-8901-2345"/>
   </div>
</div>
            
          
      
     
          <h2>Anmeldedaten</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <#if !realm.registrationEmailAsUsername>
    <div class="form-group ${messagesPerField.printIfExists('username','has-error')}">
        <div class="col-sm-2 col-md-2">
            <label for="username" class="control-label">${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>
        </div>

        <div class="col-sm-10 col-md-10">
            <input type="text" readonly="readonly" class="form-control" id="username" name="username" value="${(account.username!'')}"/>
        </div>
    </div>
</#if>
          
              <#if passwordRequired??>
<div>
</div>
</#if>

            
          
      

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">


        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label class="control-label">Profilbild</label>
            </div>

            <div class="col-sm-10 col-md-10">
                Ihr Profilbild für die Themenräume und das Ideenforum können sie <a href="https://dialog.netzwerk-universitaetsmedizin.de/profile">hier</a> aktualisieren.
            </div>
        </div>



        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <#if url.referrerURI??><a href="${url.referrerURI}">${kcSanitize(msg("backToApplication")?no_esc)}</a></#if>
                    <button type="submit" class="btn-rps-primary ${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                    <button type="submit" class="btn-rps-secondary ${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Cancel">${msg("doCancel")}</button>
                </div>
            </div>
        </div>
    </form>

</@layout.mainLayout>
