<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width" initial-scale="1">
	<!--[if !mso]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<title>Newsletter</title>
	<meta name="x-apple-disable-message-reformatting">
	<!--[if mso]>
	<style type="text/css">
		body, table, td, p {
			font-family: Arial, Helvetica, sans-serif !important;
		}
		* {
			font-family: Arial, Helvetica, sans-serif !important;
		}
	</style>
	<![endif]-->
	<!--[if !mso]><!-->
	<!-- Insert font reference, e.g. <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet"> -->
	<!--<![endif]-->
	<style type="text/css">
		*,
		*:after,
		*:before {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}

		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}

		html,
		body,
		.document {
			background-color: #EEEEEE;
			width: 100% !important;
			height: 100% !important;
			margin: 0;
			padding: 0;
		}

		body {
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
			text-rendering: optimizeLegibility;
		}

		table,
		td {
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		table {
			border-spacing: 0;
			border-collapse: collapse !important;
			table-layout: fixed;
			margin: 0 auto;
		}

		img {
			-ms-interpolation-mode: bicubic;
			max-width: 100%;
			border: 0;
			outline: none;
			text-decoration: none;
		}

		*[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
		}

		.x-gmail-data-detectors,
		.x-gmail-data-detectors *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
		}

		/* CLIENT-SPECIFIC STYLES */

		table td {
			line-height: 19px;
			font-size: 10pt;
		}

		p {
			line-height: 19px;
		}

		/* iOS BLUE LINKS */
		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		/* ANDROID CENTER FIX */
		div[style*="margin: 16px 0;"] {
			margin: 0 !important;
		}

		/******************/
		/*** GENERAL *****/
		/****************/
		* {
			font-family: Arial, Helvetica, sans-serif;
			font-size: 12px;
			color: #4D4D4D;
		}

		a {
			text-decoration: underline;
			color: #eb576a;
		}

		a:hover {
			text-decoration: underline;
		}

		img {
			border: none;
		}

		td {
			font-size: 12px;
			line-height: 18px;
		}

		h1, h2, h3, h4, h5, h6 {
			margin-top: 0px;
			margin-bottom: 0px;
		}

		h1 {
			font-size: 20px;
			font-weight: bold;
			margin-bottom: 20px;
		}

		h2 {
			font-weight: normal;
			font-size: 15px;
			text-transform: uppercase;
			margin-bottom: 5px;
		}

		h3 {
			font-size: 14px;
		}

		h4 {
			font-size: 13px;
		}

		h5 {
			font-weight: bold;
			font-size: 12px;
		}

		h6 {
			font-size: 12px;
		}

		td h2 {
			padding-top: .5em;
		}

		table.lowpriority {
			border-top: 1px solid #999999;
		}

		td.impr {
			font-size: 11px;
			line-height: 15px;
		}

		td.footertext {
			margin-top: 30px;
			padding-top: 20px;
		}

		.container {
			background-color: #ffffff;
		}

		@media all and ( max-width: 600px ) {
			.container {
				width: 100%;
				margin: auto;
			}
		}
	</style>

	<!--[if gt mso 10 ]>
	<style type="text/css">

		#body h3 {
			mso-ansi-font-size: 11pt;
		}

		.text_header h2 {
			mso-ansi-font-size: 13pt;
		}

		.contentbox .contentbox_image .teaser-pic {
			width: 180px !important;
		}

	</style>
	<![endif]-->
</head>
<body>
${kcSanitize(msg("passwordResetBodyHtml",link, linkExpiration, realmName, linkExpirationFormatter(linkExpiration)))?no_esc}
</body>