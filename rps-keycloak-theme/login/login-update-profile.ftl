<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "header">
        ${msg("loginProfileTitle")}
    <#elseif section = "form">
        <form id="kc-update-profile-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">


            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Anrede',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Anrede" class="control-label">${msg("Anrede")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    Hinterlegt: ${(user.attributes.Anrede!'')}
                                   <select class="form-control" id="user.attributes.Anrede" name="user.attributes.Anrede">
                                                       <option selected disabled hidden style='display: none' value=''></option>
                                                       <option value="Frau">Frau</option>
                                                       <option value="Herr">Herr</option>
                                                       <option value="Sonstige">Sonstige</option>
                                     </select>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Titel',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Titel" class="control-label">${msg("Titel")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Titel" name="user.attributes.Titel" value="${(user.attributes.Titel!'')}" />
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="firstName" class="${properties.kcLabelClass!}">${msg("firstName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="firstName" name="firstName" value="${(user.firstName!'')}" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="lastName" class="${properties.kcLabelClass!}">${msg("lastName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="lastName" name="lastName" value="${(user.lastName!'')}" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="email" class="${properties.kcLabelClass!}">${msg("email")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="email" name="email" readonly="readonly" value="${(user.email!'')}" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="username" class="${properties.kcLabelClass!}">${msg("username")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="username" name="username" readonly="readonly" value="${(user.username!'')}" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Ort',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Standort" class="control-label">${msg("Standort")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    Hinterlegt: ${(user.attributes.Standort!'')}
                                   <select class="form-control" id="user.attributes.Standort" name="user.attributes.Standort" value="${(user.attributes.Standort!'')}">
                                           <option selected disabled hidden style='display: none' value=''></option>
                                           <option value="Universitätsklinikum Aachen">Universitätsklinikum Aachen</option>
                                           <option value="Universitätsklinikum Augsburg">Universitätsklinikum Augsburg</option>
                                           <option value="Charité – Universitätsmedizin Berlin">Charité – Universitätsmedizin Berlin</option>
                                           <option value="Universitätsklinikum der Ruhr-Universität Bochum">Universitätsklinikum der Ruhr-Universität Bochum</option>
                                           <option value="Universitätsklinikum Bonn">Universitätsklinikum Bonn</option>
                                           <option value="Universitätsklinikum Carl Gustav Carus Dresden">Universitätsklinikum Carl Gustav Carus Dresden</option>
                                           <option value="Universitätsklinikum Düsseldorf">Universitätsklinikum Düsseldorf</option>
                                           <option value="Universitätsklinikum Erlangen">Universitätsklinikum Erlangen</option>
                                           <option value="Universitätsklinikum Essen">Universitätsklinikum Essen</option>
                                           <option value="Universitätsklinikum Frankfurt">Universitätsklinikum Frankfurt</option>
                                           <option value="Universitätsklinikum Freiburg">Universitätsklinikum Freiburg</option>
                                           <option value="Universitätsklinikum Gießen und Marburg">Universitätsklinikum Gießen und Marburg</option>
                                           <option value="Universitätsmedizin Göttingen">Universitätsmedizin Göttingen</option>
                                           <option value="Universitätsmedizin Greifswald">Universitätsmedizin Greifswald</option>
                                           <option value="Universitätsklinikum Halle (Saale)">Universitätsklinikum Halle (Saale)</option>
                                           <option value="Universitätsklinikum Hamburg-Eppendorf (UKE)">Universitätsklinikum Hamburg-Eppendorf (UKE)</option>
                                           <option value="Medizinische Hochschule Hannover">Medizinische Hochschule Hannover</option>
                                           <option value="Universitätsklinikum Heidelberg">Universitätsklinikum Heidelberg</option>
                                           <option value="Universitätsklinikum Jena">Universitätsklinikum Jena</option>
                                           <option value="Uniklinik Köln">Uniklinik Köln</option>
                                           <option value="Universitätsklinikum Leipzig">Universitätsklinikum Leipzig</option>
                                           <option value="Universitätsklinikum Magdeburg">Universitätsklinikum Magdeburg</option>
                                           <option value="Universitätsmedizin Mainz">Universitätsmedizin Mainz</option>
                                           <option value="Universitätsmedizin Mannheim">Universitätsmedizin Mannheim</option>
                                           <option value="Klinikum der Universität München (LMU Klinikum)">Klinikum der Universität München (LMU Klinikum)</option>
                                           <option value="Klinikum rechts der Isar TU München">Klinikum rechts der Isar TU München</option>
                                           <option value="Universitätsklinikum Münster">Universitätsklinikum Münster</option>
                                           <option value="Universitätsklinikum Regensburg">Universitätsklinikum Regensburg</option>
                                           <option value="Universitätsmedizin Rostock">Universitätsmedizin Rostock</option>
                                           <option value="Universitätsklinikum des Saarlandes">Universitätsklinikum des Saarlandes</option>
                                           <option value="Universitätsklinikum Schleswig-Holstein">Universitätsklinikum Schleswig-Holstein</option>
                                           <option value="Klinikum Oldenburg">Klinikum Oldenburg</option>
                                           <option value="Universitätsklinikum Tübingen">Universitätsklinikum Tübingen</option>
                                           <option value="Universitätsklinikum Ulm">Universitätsklinikum Ulm</option>
                                           <option value="Universitätsklinikum Würzburg">Universitätsklinikum Würzburg</option>
                                           <option value="Universitätsklinikum OWL (Bielefeld)">Universitätsklinikum OWL (Bielefeld)</option>
                                      </select>
                </div>
            </div>            


            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Abteilung',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Abteilung" class="control-label">${msg("Abteilung")}<sub>*</sub></label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" required class="form-control" id="user.attributes.Abteilung" name="user.attributes.Abteilung" value="${(user.attributes.Abteilung!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Telefon',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Telefon" class="control-label">${msg("Telefon")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Telefon" name="user.attributes.Telefon" value="${(user.attributes.Telefon!'')}" />
                </div>
            </div>


        <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Funktion',properties.kcFormGroupErrorClass!)}">
           <div class="${properties.kcLabelWrapperClass!}">
               <label for="user.attributes.Funktion" class="control-label">Funktion</label>
           </div>

           <div class="${properties.kcLabelWrapperClass!}">
           <input type="text" readonly="readonly" class="form-control" id="user.attributes.Funktion" name="user.attributes.Funktion" value="${(user.attributes.Telefon!'')}"/>
           </div>
        </div>
        <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.NUMProject2020',properties.kcFormGroupErrorClass!)}">
           <div class="${properties.kcLabelWrapperClass!}">
               <label for="user.attributes.NUMProject2020" class="control-label">NUM Projekt Beteiligung 1. Förderphase</label>
           </div>

           <div class="${properties.kcLabelWrapperClass!}">
               Hinterlegt: ${(user.attributes.NUMProject2020!'')}
               <select class="form-control" id="user.attributes.NUMProject2020" name="user.attributes.NUMProject2020">
                      <option selected disabled hidden style='display: none' value=''></option>
                      <option value="Übergreifend">Übergreifend</option>
                      <option value="AKTIN">AKTIN</option>
                      <option value="B-FAST">B-FAST</option>
                      <option value="CEO-sys">CEO-sys</option>
                      <option value="CODEX ">CODEX </option>
                      <option value="COMPASS">COMPASS</option>
                      <option value="COVIM ">COVIM </option>
                      <option value="DEFEAT PANDEMIcs">DEFEAT PANDEMIcs</option>
                      <option value="egePan Unimed ">egePan Unimed </option>
                      <option value="MethodCOV">MethodCOV</option>
                      <option value="NAPKON">NAPKON</option>
                      <option value="Organo-Strat">Organo-Strat</option>
                      <option value="PallPan">PallPan</option>
                      <option value="RACOON">RACOON</option>
                      <option value="None">Keine Zuordnung (Standard</option>
                    </select>
           </div>
        </div>



            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <#if isAppInitiatedAction??>
                    <input class="btn-rps-primary ${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}" />
                    <button class="btn-rps-secondary ${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" type="submit" name="cancel-aia" value="true" />${msg("doCancel")}</button>
                    <#else>
                    <input class="btn-rps-primary ${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}" />
                    </#if>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
