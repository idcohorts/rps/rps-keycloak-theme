<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
  <style>
.visible.invisible {
    visibility: hidden;
    max-height: 0;
    opacity: 0;
    transition: max-height 0.5s ease-in, opacity 0.5s ease-in;
}
.back-to-login {
    margin: 53px auto 0 auto;
    text-align: center;
}

.back-to-login a {
    color: var(--global--color-dark-gray) !important;
    font-size: 14px;
}

.pf-c-button.pf-m-primary {
    color: var(--pf-c-button--m-primary--Color);
    background-color: var(--pf-c-button--m-primary--BackgroundColor);
    
    background: var(--global--color-background, --pf-c-button--m-primary--BackgroundColor) !important;
    color: var(--global--color-dark-gray, --pf-c-button--m-primary--Color) !important;
    border-style: solid;
    border-width: 2px;
    font-weight: 700;
    border-color: var(--global--color-dark-gray, --pf-c-button--m-primary--Color) !important;
    border-radius: 5px;
    box-shadow: 0px 0px 5px 0px rgb(255 255 255 / 50%);
    text-decoration: none;
  }

.pf-c-button.pf-m-primary:hover {
    background: var(--global--color-dark-gray, --pf-c-button--m-primary--BackgroundColor) !important;
    color: var(--global--color-background, --pf-c-button--m-primary--Color) !important;
}

.visible {
    visibility: visible;
    opacity: 1;
    transition: max-height 0.5s ease-in, opacity 0.5s ease-in;
    max-height: 400em;
}

/* Mark input boxes that gets an error on validation: */
select.invalid,
input.invalid {
    background-color: #ffdddd;
}

.d-none {
    display: none;
}

ul.form-stepper {
    counter-reset: section;
    margin-bottom: 3rem;
}
ul.form-stepper .form-stepper-circle {
    position: relative;
}
ul.form-stepper .form-stepper-circle span {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateY(-50%) translateX(-50%);
}
.form-stepper-horizontal {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
}
ul.form-stepper > li:not(:last-of-type) {
    margin-bottom: 0.625rem;
    -webkit-transition: margin-bottom 0.4s;
    -o-transition: margin-bottom 0.4s;
    transition: margin-bottom 0.4s;
}
.form-stepper-horizontal > li:not(:last-of-type) {
    margin-bottom: 0 !important;
}
.form-stepper-horizontal li {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: start;
    -webkit-transition: 0.5s;
    transition: 0.5s;
}
.form-stepper-horizontal li:not(:last-child):after {
    position: relative;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    height: 1px;
    content: "";
    top: 49%; /* => fixes rendering bug */
}
.form-stepper-horizontal li:after {
    background-color: #dee2e6;
}
.form-stepper-horizontal li.form-stepper-completed:after {
    background-color: var(--global--color-primary, #4361ee) !important;
}
.form-stepper-horizontal li:last-child {
    flex: unset;
}
ul.form-stepper li a .form-stepper-circle {
    display: inline-block;
    width: 40px;
    height: 40px;
    margin-right: 0;
    line-height: 1.7rem;
    text-align: center;
    background: rgba(0, 0, 0, 0.38);
    border-radius: 50%;
}
.form-stepper .form-stepper-active .form-stepper-circle {
    background-color: var(--global--color-primary, #4361ee) !important;
    color: #fff;
}
.form-stepper .form-stepper-active .label {
    color: var(--global--color-primary, #4361ee) !important;
}
.form-stepper .form-stepper-active .form-stepper-circle:hover {
    background-color: var(--global--color-primary, #4361ee) !important;
    color: #fff !important;
}
.form-stepper .form-stepper-unfinished .form-stepper-circle {
    background-color: #f8f7ff;
}
.form-stepper .form-stepper-completed .form-stepper-circle {
    background-color: var(--global--color-secondary, #0e9594) !important;
    color: #fff;
}
.form-stepper .form-stepper-completed .label {
    color: var(--global--color-secondary, #0e9594) !important;
}
.form-stepper .form-stepper-completed .form-stepper-circle:hover {
    background-color: var(--global--color-secondary, #0e9594) !important;
    color: #fff !important;
}
.form-stepper .form-stepper-active span.text-muted {
    color: #fff !important;
}
.form-stepper .form-stepper-completed span.text-muted {
    color: #fff !important;
}
.form-stepper .label {
    font-size: 1rem;
    margin-top: 0.5rem;
}
.form-stepper a {
    cursor: default;
}</style>
  <style>

  body{
    background-color: rgb(234, 237, 242) !important;
  }
  @media (min-width: 768px) {
      .login-pf-page .card-pf {
        padding: 20px 40px 30px 40px;
        margin-top: 0px;
        border-radius: 5px;
    }
  }
  @media (max-width: 767px) {
    .login-pf-page .card-pf {
      margin-top: 160px;
      margin-left: auto;
      margin-right: auto;
    }
    .register-title{
      padding-top: 20px;
      margin: auto 0px;
      padding-left: 10px;
      padding-right: 10px;
    }
  }
  .register-header-text {
    position: absolute;
    top: 206px;
    font-size: 17px;
    max-width: 500px;
    font-weight: 400;
    color: #44536a;
  }
  .header-num-logo{
    margin: 0 auto !important;
    background-color: #ffff !important;
    padding: 20px !important;
    border-radius: 5px !important;
    max-width: 600px !important;
    text-align: center;
  }
  .btn-rps-primary{
    height: 42px;
    transition: 0.4s;
  }
  .alert-info-header{
    color: #004085;
    background-color: #cce5ff;
    border-color: #b8daff;
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
    font-size: 15px;
    font-weight: 500;
  }

  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <#if section = "header">
   <div class="alert alert-info-header"> ${msg("registerHeader")}</div> 
   <!-- <div class="register-header-text alert alert-primary"> ${msg("registerHeader")}</div>-->
   <div class="register-title"> ${msg("registerTitle")}</div> 
  <#elseif section = "form">
  <!-- Form Steps / Progress Bar -->
      <ul class="form-stepper form-stepper-horizontal text-center mx-auto pl-0">
        
            <li id="step-header-1" class="form-stepper-active  text-center form-stepper-list" step="1">
                <a class="mx-2">
                    <span class="form-stepper-circle">
                        <span>1</span>
                    </span>
                </a>
            </li>
        
            <li id="step-header-2" class="form-stepper-unfinished text-center form-stepper-list" step="2">
                <a class="mx-2">
                    <span class="form-stepper-circle">
                        <span>2</span>
                    </span>
                </a>
            </li>
        
            <li id="step-header-3" class="form-stepper-unfinished text-center form-stepper-list" step="3">
                <a class="mx-2">
                    <span class="form-stepper-circle">
                        <span>3</span>
                    </span>
                </a>
            </li>
        
            <li id="step-header-4" class="form-stepper-unfinished text-center form-stepper-list" step="4">
                <a class="mx-2">
                    <span class="form-stepper-circle">
                        <span>4</span>
                    </span>
                </a>
            </li>
        
            <li id="step-header-5" class="form-stepper-unfinished text-center form-stepper-list" step="5">
                <a class="mx-2">
                    <span class="form-stepper-circle">
                        <span>5</span>
                    </span>
                </a>
            </li>
        
        </ul>
    <form id="kc-register-form" class="${properties.kcFormClass!}" action="${url.registrationAction}" method="post" style="top:300px;">
      
        <section id="step-1" class="form-step">
          <h2>Persönliche Angaben</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div id="user.attributes.form_of_address.div" 
     class="form-group
       ">
  <div id="user.attributes.form_of_address.div"  
       class="${properties.kcInputWrapperClass!}">
    Anrede
    
      <sub>*</sub>
    
    
    <select  required 
        class="${properties.kcInputClass!}" id="user.attributes.form_of_address" 
        name="user.attributes.form_of_address" 
        value="${(register.formData['user.attributes.form_of_address']!'')}" 
        onchange='apply_rules(this); set_other({"attribute": "form_of_address", "default": "W\u00e4hlen Sie eine von Ihnen gew\u00fcnschte Anrede aus", "description": "Wir nutzen die Anrede, um Sie in unseren E-Mails korrekt anzusprechen.", "options": [{"description": "Sehr geehrte Frau", "value": "Sehr geehrte Frau"}, {"description": "Sehr geehrter Herr", "value": "Sehr geehrter Herr"}, {"description": "Sehr geehrte*", "value": "Sehr geehrte*"}, {"description": "Guten Tag", "value": "Guten Tag"}], "required": true, "title": "Anrede", "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
      
        <option selected disabled hidden style='display: none' value=''>Wählen Sie eine von Ihnen gewünschte Anrede aus</option>
      
      
        <option id="user.attributes.form_of_address.Sehr geehrte Frau" 
                value="Sehr geehrte Frau"
                class="">
          Sehr geehrte Frau
        </option>
      
        <option id="user.attributes.form_of_address.Sehr geehrter Herr" 
                value="Sehr geehrter Herr"
                class="">
          Sehr geehrter Herr
        </option>
      
        <option id="user.attributes.form_of_address.Sehr geehrte*" 
                value="Sehr geehrte*"
                class="">
          Sehr geehrte*
        </option>
      
        <option id="user.attributes.form_of_address.Guten Tag" 
                value="Guten Tag"
                class="">
          Guten Tag
        </option>
      
      
    </select>
  </div>
</div>

          
              <div class="form-group">
   <div class="${properties.kcInputWrapperClass!}">
    Titel
    
      
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.title" 
        name="user.attributes.title" 
        value="${(register.formData['user.attributes.title']!'')}"  
        placeholder="z.B. Dr., Prof., PD"/>
   </div>
</div>

          
              <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
    <div class="${properties.kcInputWrapperClass!}">${msg("firstName")} <sub>*</sub>
      
        <input required require type="text" 
            id="firstName" 
            class="${properties.kcInputClass!}" 
            name="firstName" 
            value="${(register.formData.firstName!'')}"
            onchange="set_user_name()" />
    </div>
</div>

          
              <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
    <div class="${properties.kcInputWrapperClass!}">${msg("lastName")} <sub>*</sub>
      
        <input required type="text" 
            id="lastName" 
            class="${properties.kcInputClass!}" 
            name="lastName" 
            value="${(register.formData.lastName!'')}" 
            onchange="set_user_name()"/>
    </div>
</div>

          
              <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
    <div class="${properties.kcLabelWrapperClass!}">
        <label for="email" class="${properties.kcLabelClass!}">E-Mail (Beruflich)</label> <sub>*</sub>
      
    </div>
    <div class="${properties.kcInputWrapperClass!}">
        <input required type="text" id="email" 
            class="${properties.kcInputClass!}" 
            name="email" value="${(register.formData.email!'')}" 
            autocomplete="email" />
    </div>
</div>

          
              <div class="form-group">
   <div class="${properties.kcInputWrapperClass!}">
    Telefonnummer (Beruflich)
    
      
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.phone" 
        name="user.attributes.phone" 
        value="${(register.formData['user.attributes.phone']!'')}"  
        />
   </div>
</div>

                  
          <div class="mt-3">
              
              <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6"
                 style="display:none" >
                <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(-1)"><i class="fa-solid fa-left"></i>Zurück</button>
              </div>
                            
                <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(1)">Weiter <i class="fa-solid fa-right"></i></button>
                </div>                
              

          </div>
        </section>
      
        <section id="step-2" class="form-step d-none">
          <h2>Berufliche Angaben</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div id="user.attributes.organisation_type.div" 
     class="form-group
       ">
  <div id="user.attributes.organisation_type.div"  
       class="${properties.kcInputWrapperClass!}">
    Organisation
    
      <sub>*</sub>
    
    
    <select  required 
        class="${properties.kcInputClass!}" id="user.attributes.organisation_type" 
        name="user.attributes.organisation_type" 
        value="${(register.formData['user.attributes.organisation_type']!'')}" 
        onchange='apply_rules(this); set_other({"attribute": "organisation_type", "default": "W\u00e4hlen Sie Ihre Organisation aus.", "options": [{"description": "Uniklinik", "value": "Uniklinik"}, {"description": "Nicht-universit\u00e4re Partnereinrichtung", "value": "Nicht-universit\u00e4re Partnereinrichtung"}], "required": true, "title": "Organisation", "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
      
        <option selected disabled hidden style='display: none' value=''>Wählen Sie Ihre Organisation aus.</option>
      
      
        <option id="user.attributes.organisation_type.Uniklinik" 
                value="Uniklinik"
                class="">
          Uniklinik
        </option>
      
        <option id="user.attributes.organisation_type.Nicht-universitäre Partnereinrichtung" 
                value="Nicht-universitäre Partnereinrichtung"
                class="">
          Nicht-universitäre Partnereinrichtung
        </option>
      
      
    </select>
  </div>
</div>

          
              <div id="user.attributes.Organisation.div" 
     class="form-group
        visible invisible ">
  <div id="user.attributes.Organisation.div"  
       class="${properties.kcInputWrapperClass!}">
    None
    
    
    <select  required 
        class="${properties.kcInputClass!}" id="user.attributes.Organisation" 
        name="user.attributes.Organisation" 
        value="${(register.formData['user.attributes.Organisation']!'')}" 
        onchange='apply_rules(this); set_other({"attribute": "Organisation", "default": "W\u00e4hlen Sie Ihre Organisation aus.", "hidden": true, "options": [{"description": "Uniklinik RWTH Aachen", "value": "num-organisations--rwth-aachen"}, {"description": "Universit\u00e4tsklinikum Augsburg", "value": "num-organisations--uk-augsburg"}, {"description": "Charit\u00e9 - Universit\u00e4tsmedizin Berlin", "value": "num-organisations--charite-berlin"}, {"description": "Universit\u00e4tsklinikum OWL", "value": "num-organisations--uk-owl"}, {"description": "Universit\u00e4tsklinikum der Ruhr-Universit\u00e4t Bochum", "value": "num-organisations--uk-bochum"}, {"description": "Universit\u00e4tsklinikum Bonn", "value": "num-organisations--uk-bonn"}, {"description": "Universit\u00e4tsklinikum Carl Gustav Carus Dresden", "value": "num-organisations--uk-dresden"}, {"description": "Universit\u00e4tsklinikum D\u00fcsseldorf", "value": "num-organisations--uk-duesseldorf"}, {"description": "Universit\u00e4tsklinikum Erlangen", "value": "num-organisations--uk-erlangen"}, {"description": "Universit\u00e4tsklinikum Essen", "value": "num-organisations--uk-essen"}, {"description": "Universit\u00e4tsklinikum Frankfurt", "value": "num-organisations--uk-frankfurt"}, {"description": "Universit\u00e4tsklinikum Freiburg", "value": "num-organisations--uk-freiburg"}, {"description": "Uniklinikum Gie\u00dfen und Marburg", "value": "num-organisations--uk-marburg"}, {"description": "Universit\u00e4tsmedizin G\u00f6ttingen", "value": "num-organisations--uk-goettingen"}, {"description": "Universit\u00e4tsmedizin Greifswald", "value": "num-organisations--uk-greifswald"}, {"description": "Universit\u00e4tsklinikum Halle (Saale)", "value": "num-organisations--uk-halle"}, {"description": "Universit\u00e4tsklinikum Hamburg-Eppendorf", "value": "num-organisations--uk-hamburg-eppendorf"}, {"description": "Medizinische Hochschule Hannover", "value": "num-organisations--mh-hannover"}, {"description": "Universit\u00e4tsklinikum Heidelberg", "value": "num-organisations--uk-heidelberg"}, {"description": "Universit\u00e4tsklinikum des Saarlandes", "value": "num-organisations--uk-saarland"}, {"description": "Universit\u00e4tsklinikum Jena", "value": "num-organisations--uk-jena"}, {"description": "Uniklinik K\u00f6ln", "value": "num-organisations--uk-koeln"}, {"description": "Universit\u00e4tsklinikum Leipzig", "value": "num-organisations--uk-leipzig"}, {"description": "Universit\u00e4tsmedizin Magdeburg", "value": "num-organisations--uk-magdeburg"}, {"description": "Universit\u00e4tsmedizin Mainz", "value": "num-organisations--uk-mainz"}, {"description": "Universit\u00e4tsmedizin Mannheim", "value": "num-organisations--uk-mannheim"}, {"description": "LMU Klinikum M\u00fcnchen", "value": "num-organisations--lmu-muenchen"}, {"description": "Klinikum rechts der Isar M\u00fcnchen", "value": "num-organisations--k-recht-der-isar-muenchen"}, {"description": "Universit\u00e4tsklinikum M\u00fcnster", "value": "num-organisations--uk-muenster"}, {"description": "Universit\u00e4tsmedizin Oldenburg", "value": "num-organisations--uk-oldenburg"}, {"description": "Universit\u00e4tsklinikum Regensburg", "value": "num-organisations--uk-regensburg"}, {"description": "Universit\u00e4tsmedizin Rostock", "value": "num-organisations--uk-rostock"}, {"description": "Universit\u00e4tsklinikum Schleswig-Holstein", "value": "num-organisations--uk-schleswig-holstein"}, {"description": "Universit\u00e4tsklinikum T\u00fcbingen", "value": "num-organisations--uk-tuebingen"}, {"description": "Universit\u00e4tsklinikum Ulm", "value": "num-organisations--uk-ulm"}, {"description": "Universit\u00e4tsklinikum W\u00fcrzburg", "value": "num-organisations--uk-wuerzburg"}, {"description": "Carl-Thiem Klinikum Cottbus", "value": "num-organisations--cottbus"}, {"description": "DKFZ", "value": "num-organisations--dkfz"}, {"description": "Fraunhofer MEVIS", "value": "num-organisations--fraunhofer-mevis"}, {"description": "HIGHMED", "value": "num-organisations--highmed"}, {"description": "Helmholtz-Zentrum M\u00fcnchen (HMGU)", "value": "num-organisations--hmgu"}, {"description": "Helmholtz-Zentrum f\u00fcr Infektionsforschung (HZI)", "value": "num-organisations--hzi"}, {"description": "Technische Hochschule Heilbronn", "value": "num-organisations--ths-heilbronn"}, {"description": "TMF", "value": "num-organisations--tmf"}, {"description": "TU Darmstadt", "value": "num-organisations--tu-darmstadt"}, {"description": "Liste wird kommen", "value": "num-organisations--other"}], "required": true, "title": null, "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
      
        <option selected disabled hidden style='display: none' value=''>Wählen Sie Ihre Organisation aus.</option>
      
      
        <option id="user.attributes.Organisation.num-organisations--rwth-aachen" 
                value="num-organisations--rwth-aachen"
                class=" hidden ">
          Uniklinik RWTH Aachen
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-augsburg" 
                value="num-organisations--uk-augsburg"
                class=" hidden ">
          Universitätsklinikum Augsburg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--charite-berlin" 
                value="num-organisations--charite-berlin"
                class=" hidden ">
          Charité - Universitätsmedizin Berlin
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-owl" 
                value="num-organisations--uk-owl"
                class=" hidden ">
          Universitätsklinikum OWL
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-bochum" 
                value="num-organisations--uk-bochum"
                class=" hidden ">
          Universitätsklinikum der Ruhr-Universität Bochum
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-bonn" 
                value="num-organisations--uk-bonn"
                class=" hidden ">
          Universitätsklinikum Bonn
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-dresden" 
                value="num-organisations--uk-dresden"
                class=" hidden ">
          Universitätsklinikum Carl Gustav Carus Dresden
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-duesseldorf" 
                value="num-organisations--uk-duesseldorf"
                class=" hidden ">
          Universitätsklinikum Düsseldorf
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-erlangen" 
                value="num-organisations--uk-erlangen"
                class=" hidden ">
          Universitätsklinikum Erlangen
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-essen" 
                value="num-organisations--uk-essen"
                class=" hidden ">
          Universitätsklinikum Essen
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-frankfurt" 
                value="num-organisations--uk-frankfurt"
                class=" hidden ">
          Universitätsklinikum Frankfurt
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-freiburg" 
                value="num-organisations--uk-freiburg"
                class=" hidden ">
          Universitätsklinikum Freiburg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-marburg" 
                value="num-organisations--uk-marburg"
                class=" hidden ">
          Uniklinikum Gießen und Marburg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-goettingen" 
                value="num-organisations--uk-goettingen"
                class=" hidden ">
          Universitätsmedizin Göttingen
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-greifswald" 
                value="num-organisations--uk-greifswald"
                class=" hidden ">
          Universitätsmedizin Greifswald
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-halle" 
                value="num-organisations--uk-halle"
                class=" hidden ">
          Universitätsklinikum Halle (Saale)
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-hamburg-eppendorf" 
                value="num-organisations--uk-hamburg-eppendorf"
                class=" hidden ">
          Universitätsklinikum Hamburg-Eppendorf
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--mh-hannover" 
                value="num-organisations--mh-hannover"
                class=" hidden ">
          Medizinische Hochschule Hannover
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-heidelberg" 
                value="num-organisations--uk-heidelberg"
                class=" hidden ">
          Universitätsklinikum Heidelberg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-saarland" 
                value="num-organisations--uk-saarland"
                class=" hidden ">
          Universitätsklinikum des Saarlandes
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-jena" 
                value="num-organisations--uk-jena"
                class=" hidden ">
          Universitätsklinikum Jena
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-koeln" 
                value="num-organisations--uk-koeln"
                class=" hidden ">
          Uniklinik Köln
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-leipzig" 
                value="num-organisations--uk-leipzig"
                class=" hidden ">
          Universitätsklinikum Leipzig
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-magdeburg" 
                value="num-organisations--uk-magdeburg"
                class=" hidden ">
          Universitätsmedizin Magdeburg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-mainz" 
                value="num-organisations--uk-mainz"
                class=" hidden ">
          Universitätsmedizin Mainz
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-mannheim" 
                value="num-organisations--uk-mannheim"
                class=" hidden ">
          Universitätsmedizin Mannheim
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--lmu-muenchen" 
                value="num-organisations--lmu-muenchen"
                class=" hidden ">
          LMU Klinikum München
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--k-recht-der-isar-muenchen" 
                value="num-organisations--k-recht-der-isar-muenchen"
                class=" hidden ">
          Klinikum rechts der Isar München
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-muenster" 
                value="num-organisations--uk-muenster"
                class=" hidden ">
          Universitätsklinikum Münster
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-oldenburg" 
                value="num-organisations--uk-oldenburg"
                class=" hidden ">
          Universitätsmedizin Oldenburg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-regensburg" 
                value="num-organisations--uk-regensburg"
                class=" hidden ">
          Universitätsklinikum Regensburg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-rostock" 
                value="num-organisations--uk-rostock"
                class=" hidden ">
          Universitätsmedizin Rostock
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-schleswig-holstein" 
                value="num-organisations--uk-schleswig-holstein"
                class=" hidden ">
          Universitätsklinikum Schleswig-Holstein
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-tuebingen" 
                value="num-organisations--uk-tuebingen"
                class=" hidden ">
          Universitätsklinikum Tübingen
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-ulm" 
                value="num-organisations--uk-ulm"
                class=" hidden ">
          Universitätsklinikum Ulm
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--uk-wuerzburg" 
                value="num-organisations--uk-wuerzburg"
                class=" hidden ">
          Universitätsklinikum Würzburg
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--cottbus" 
                value="num-organisations--cottbus"
                class=" hidden ">
          Carl-Thiem Klinikum Cottbus
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--dkfz" 
                value="num-organisations--dkfz"
                class=" hidden ">
          DKFZ
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--fraunhofer-mevis" 
                value="num-organisations--fraunhofer-mevis"
                class=" hidden ">
          Fraunhofer MEVIS
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--highmed" 
                value="num-organisations--highmed"
                class=" hidden ">
          HIGHMED
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--hmgu" 
                value="num-organisations--hmgu"
                class=" hidden ">
          Helmholtz-Zentrum München (HMGU)
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--hzi" 
                value="num-organisations--hzi"
                class=" hidden ">
          Helmholtz-Zentrum für Infektionsforschung (HZI)
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--ths-heilbronn" 
                value="num-organisations--ths-heilbronn"
                class=" hidden ">
          Technische Hochschule Heilbronn
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--tmf" 
                value="num-organisations--tmf"
                class=" hidden ">
          TMF
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--tu-darmstadt" 
                value="num-organisations--tu-darmstadt"
                class=" hidden ">
          TU Darmstadt
        </option>
      
        <option id="user.attributes.Organisation.num-organisations--other" 
                value="num-organisations--other"
                class=" hidden ">
          Liste wird kommen
        </option>
      
      
    </select>
  </div>
</div>

          
              <div class="form-group">
   <div class="${properties.kcInputWrapperClass!}">
    Bereich <sub>*</sub>
    
      
       <input type="text" required 
        class="${properties.kcInputClass!}" 
        id="user.attributes.department" 
        name="user.attributes.department" 
        value="${(register.formData['user.attributes.department']!'')}"  
        placeholder="z.B. Abteilung, Institut, Geschäftsstelle, Referat, Klinik"/>
   </div>
</div>

          
              <div class="form-group">
   <div class="${properties.kcInputWrapperClass!}">
    Funktion
    
      
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.function" 
        name="user.attributes.function" 
        value="${(register.formData['user.attributes.function']!'')}"  
        placeholder="z.B. Leiter Arbeitsgruppe, Assistenzarzt, Referent, Mitarbeiter Drittmittelverwaltung, Study Nurse"/>
   </div>
</div>

                  
          <div class="mt-3">
              
              <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6"
                 style="display:visible" >
                <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(-1)"><i class="fa-solid fa-left"></i>Zurück</button>
              </div>
                            
                <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(1)">Weiter <i class="fa-solid fa-right"></i></button>
                </div>                
              

          </div>
        </section>
      
        <section id="step-3" class="form-step d-none">
          <h2>Angaben in Bezug zum Netzwerk Universitätsmedizin</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div class="form-group">
  <div id="user.attributes.NUM-Funktion.div"  
       class="${properties.kcInputWrapperClass!} ">
      Primäre Rolle im Netzwerk Universitätsmedizin
        
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--loks-l" 
            name="user.attributes.num-roles--loks-l" 
            id="user.attributes.num-roles--loks-l" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--loks-l">Leiter*in Lokale Stabsstelle</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--loks-m" 
            name="user.attributes.num-roles--loks-m" 
            id="user.attributes.num-roles--loks-m" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--loks-m">Mitarbeiter*in Lokale Stabsstelle</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--dmv" 
            name="user.attributes.num-roles--dmv" 
            id="user.attributes.num-roles--dmv" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--dmv">Team Drittmittelverwaltung</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--tp-l" 
            name="user.attributes.num-roles--tp-l" 
            id="user.attributes.num-roles--tp-l" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--tp-l">Teilprojektleiter*in</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--tp-m" 
            name="user.attributes.num-roles--tp-m" 
            id="user.attributes.num-roles--tp-m" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--tp-m">Teilprojektmitarbeiter*in</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--ks" 
            name="user.attributes.num-roles--ks" 
            id="user.attributes.num-roles--ks" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--ks">Koordinierungsstelle</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--antrs" 
            name="user.attributes.num-roles--antrs" 
            id="user.attributes.num-roles--antrs" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--antrs">NUM Antragsteller*in</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--kp" 
            name="user.attributes.num-roles--kp" 
            id="user.attributes.num-roles--kp" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--kp">Kooperationspartner*in in einem NUM-Projekt</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--uac" 
            name="user.attributes.num-roles--uac" 
            id="user.attributes.num-roles--uac" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--uac">Antragsteller*in Use and Access</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-roles--gremien" 
            name="user.attributes.num-roles--gremien" 
            id="user.attributes.num-roles--gremien" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-roles--gremien">Miarbeiter*in Gremienportal</label>
    </div>
  
    </div>
</div>


          
              <div class="form-group">
  <div id="user.attributes.Projektteilnahme.div"  
       class="${properties.kcInputWrapperClass!} ">
      Beteiligung an den NUM-Projekten
        
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects-project-type--infrastrukturlinie" 
            name="user.attributes.num-projects-project-type--infrastrukturlinie" 
            id="user.attributes.num-projects-project-type--infrastrukturlinie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects-project-type--infrastrukturlinie">Infrastrukturlinie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects-project-type--forschungslinie" 
            name="user.attributes.num-projects-project-type--forschungslinie" 
            id="user.attributes.num-projects-project-type--forschungslinie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects-project-type--forschungslinie">Forschungslinie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--none" 
            name="user.attributes.num-projects--none" 
            id="user.attributes.num-projects--none" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--none">Keine Beteiligung</label>
    </div>
  
    </div>
</div>


          
              <div class="form-group">
  <div id="user.attributes.Infrastrukturlinie.div"  
       class="${properties.kcInputWrapperClass!}  visible invisible ">
      Beteiligung an den NUM-Projekten der Infrastrukturlinie
        
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--atkin" 
            name="user.attributes.num-projects--atkin" 
            id="user.attributes.num-projects--atkin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--atkin">AKTIN</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--gensurv" 
            name="user.attributes.num-projects--gensurv" 
            id="user.attributes.num-projects--gensurv" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--gensurv">GenSurv</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--nukleus" 
            name="user.attributes.num-projects--nukleus" 
            id="user.attributes.num-projects--nukleus" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--nukleus">NUKLEUS</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--num-rdp" 
            name="user.attributes.num-projects--num-rdp" 
            id="user.attributes.num-projects--num-rdp" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--num-rdp">NUM-RDP</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--racoon" 
            name="user.attributes.num-projects--racoon" 
            id="user.attributes.num-projects--racoon" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--racoon">RACOON</label>
    </div>
  
    </div>
</div>


          
              <div class="form-group">
  <div id="user.attributes.Forschungslinie.div"  
       class="${properties.kcInputWrapperClass!}  visible invisible ">
      Beteiligung an den NUM-Projekten der Forschungslinie
        
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--aktin-2-0" 
            name="user.attributes.num-projects--aktin-2-0" 
            id="user.attributes.num-projects--aktin-2-0" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--aktin-2-0">AKTIN 2.0</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--codexplus" 
            name="user.attributes.num-projects--codexplus" 
            id="user.attributes.num-projects--codexplus" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--codexplus">CODEX+</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--collpan" 
            name="user.attributes.num-projects--collpan" 
            id="user.attributes.num-projects--collpan" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--collpan">CollPan</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--coverchild" 
            name="user.attributes.num-projects--coverchild" 
            id="user.attributes.num-projects--coverchild" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--coverchild">COVerCHILD</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--covim" 
            name="user.attributes.num-projects--covim" 
            id="user.attributes.num-projects--covim" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--covim">COVIM</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--moltrax" 
            name="user.attributes.num-projects--moltrax" 
            id="user.attributes.num-projects--moltrax" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--moltrax">MolTrax</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--napkon-tip" 
            name="user.attributes.num-projects--napkon-tip" 
            id="user.attributes.num-projects--napkon-tip" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--napkon-tip">NAPKON TIP</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--napkon" 
            name="user.attributes.num-projects--napkon" 
            id="user.attributes.num-projects--napkon" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--napkon">NAPKON</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--prepared" 
            name="user.attributes.num-projects--prepared" 
            id="user.attributes.num-projects--prepared" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--prepared">PREPARED</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--racoon-combined" 
            name="user.attributes.num-projects--racoon-combined" 
            id="user.attributes.num-projects--racoon-combined" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--racoon-combined">RACOON COMBINE</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-projects--utn-telemedizin" 
            name="user.attributes.num-projects--utn-telemedizin" 
            id="user.attributes.num-projects--utn-telemedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-projects--utn-telemedizin">UTN-Telemedizin</label>
    </div>
  
    </div>
</div>


                  
          <div class="mt-3">
              
              <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6"
                 style="display:visible" >
                <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(-1)"><i class="fa-solid fa-left"></i>Zurück</button>
              </div>
                            
                <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(1)">Weiter <i class="fa-solid fa-right"></i></button>
                </div>                
              

          </div>
        </section>
      
        <section id="step-4" class="form-step d-none">
          <h2>Angaben in Bezug zum Netzwerk Universitätsmedizin</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <div id="user.attributes.fosa-beteiligung.div" 
     class="form-group
       ">
  <div id="user.attributes.fosa-beteiligung.div"  
       class="${properties.kcInputWrapperClass!}">
    Beteiligung an den FOSAs
    
      <sub>*</sub>
    
    
    <select  required 
        class="${properties.kcInputClass!}" id="user.attributes.fosa-beteiligung" 
        name="user.attributes.fosa-beteiligung" 
        value="${(register.formData['user.attributes.fosa-beteiligung']!'')}" 
        onchange='apply_rules(this); set_other({"attribute": "fosa-beteiligung", "default": "W\u00e4hlen Sie aus...", "options": [{"description": "Ja", "value": "ja-fosa-beteiligung"}, {"description": "Keine Beteiligung", "value": "none-fosa-beteiligung"}], "required": true, "title": "Beteiligung an den FOSAs", "type": "dropdown"}, this, "${properties.kcInputClass!}");''>
      
        <option selected disabled hidden style='display: none' value=''>Wählen Sie aus...</option>
      
      
        <option id="user.attributes.fosa-beteiligung.ja-fosa-beteiligung" 
                value="ja-fosa-beteiligung"
                class="">
          Ja
        </option>
      
        <option id="user.attributes.fosa-beteiligung.none-fosa-beteiligung" 
                value="none-fosa-beteiligung"
                class="">
          Keine Beteiligung
        </option>
      
      
    </select>
  </div>
</div>

          
              <div class="form-group">
  <div id="user.attributes.Fachbereich.div"  
       class="${properties.kcInputWrapperClass!}  visible invisible ">
      Fachbereich
        
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--allgemeinmedizin" 
            name="user.attributes.num-fosa--allgemeinmedizin" 
            id="user.attributes.num-fosa--allgemeinmedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--allgemeinmedizin">Allgemeinmedizin</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--anaesthesiologie" 
            name="user.attributes.num-fosa--anaesthesiologie" 
            id="user.attributes.num-fosa--anaesthesiologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--anaesthesiologie">Anästhesiologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--dermatologie" 
            name="user.attributes.num-fosa--dermatologie" 
            id="user.attributes.num-fosa--dermatologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--dermatologie">Dermatologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--epidemiologie-public-health" 
            name="user.attributes.num-fosa--epidemiologie-public-health" 
            id="user.attributes.num-fosa--epidemiologie-public-health" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--epidemiologie-public-health">Epidemiologie & Public Health</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--gastroenterologie" 
            name="user.attributes.num-fosa--gastroenterologie" 
            id="user.attributes.num-fosa--gastroenterologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--gastroenterologie">Gastroenterologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--geriatrie" 
            name="user.attributes.num-fosa--geriatrie" 
            id="user.attributes.num-fosa--geriatrie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--geriatrie">Geriatrie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--gynaekologie-und-geburtshilfe" 
            name="user.attributes.num-fosa--gynaekologie-und-geburtshilfe" 
            id="user.attributes.num-fosa--gynaekologie-und-geburtshilfe" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--gynaekologie-und-geburtshilfe">Gynäkologie und Geburtshilfe</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--haematologie-onkologie" 
            name="user.attributes.num-fosa--haematologie-onkologie" 
            id="user.attributes.num-fosa--haematologie-onkologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--haematologie-onkologie">Hämatologie/Onkologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--hno" 
            name="user.attributes.num-fosa--hno" 
            id="user.attributes.num-fosa--hno" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--hno">HNO</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--humangenetik" 
            name="user.attributes.num-fosa--humangenetik" 
            id="user.attributes.num-fosa--humangenetik" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--humangenetik">Humangenetik</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--immunologie-autoimmunitaet" 
            name="user.attributes.num-fosa--immunologie-autoimmunitaet" 
            id="user.attributes.num-fosa--immunologie-autoimmunitaet" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--immunologie-autoimmunitaet">Immunologie & Autoimmunität</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--infektologie" 
            name="user.attributes.num-fosa--infektologie" 
            id="user.attributes.num-fosa--infektologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--infektologie">Infektologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--intensivmedizin" 
            name="user.attributes.num-fosa--intensivmedizin" 
            id="user.attributes.num-fosa--intensivmedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--intensivmedizin">Intensivmedizin</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--kardiologie" 
            name="user.attributes.num-fosa--kardiologie" 
            id="user.attributes.num-fosa--kardiologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--kardiologie">Kardiologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--laboratoriumsmedizin" 
            name="user.attributes.num-fosa--laboratoriumsmedizin" 
            id="user.attributes.num-fosa--laboratoriumsmedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--laboratoriumsmedizin">Laboratoriumsmedizin</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--mkg-zahnmedizin" 
            name="user.attributes.num-fosa--mkg-zahnmedizin" 
            id="user.attributes.num-fosa--mkg-zahnmedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--mkg-zahnmedizin">MKG & Zahnmedizin</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--nephrologie" 
            name="user.attributes.num-fosa--nephrologie" 
            id="user.attributes.num-fosa--nephrologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--nephrologie">Nephrologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--neurologie" 
            name="user.attributes.num-fosa--neurologie" 
            id="user.attributes.num-fosa--neurologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--neurologie">Neurologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--neuroradiologie" 
            name="user.attributes.num-fosa--neuroradiologie" 
            id="user.attributes.num-fosa--neuroradiologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--neuroradiologie">Neuroradiologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--notfallmedizin" 
            name="user.attributes.num-fosa--notfallmedizin" 
            id="user.attributes.num-fosa--notfallmedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--notfallmedizin">Notfallmedizin</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--paediatrie" 
            name="user.attributes.num-fosa--paediatrie" 
            id="user.attributes.num-fosa--paediatrie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--paediatrie">Pädiatrie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--palliativmedizin" 
            name="user.attributes.num-fosa--palliativmedizin" 
            id="user.attributes.num-fosa--palliativmedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--palliativmedizin">Palliativmedizin</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--pneumologie" 
            name="user.attributes.num-fosa--pneumologie" 
            id="user.attributes.num-fosa--pneumologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--pneumologie">Pneumologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--psychische-gesundheit" 
            name="user.attributes.num-fosa--psychische-gesundheit" 
            id="user.attributes.num-fosa--psychische-gesundheit" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--psychische-gesundheit">Psychische-Gesundheit</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--radiologie" 
            name="user.attributes.num-fosa--radiologie" 
            id="user.attributes.num-fosa--radiologie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--radiologie">Radiologie</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--rehabilitation" 
            name="user.attributes.num-fosa--rehabilitation" 
            id="user.attributes.num-fosa--rehabilitation" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--rehabilitation">Rehabilitation</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--schmerzmedizin" 
            name="user.attributes.num-fosa--schmerzmedizin" 
            id="user.attributes.num-fosa--schmerzmedizin" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--schmerzmedizin">Schmerzmedizin</label>
    </div>
  
    <div class="${properties.kcInputWrapperClass!}">
      <input type="checkbox" id="user.attributes.num-fosa--strahlentherapie" 
            name="user.attributes.num-fosa--strahlentherapie" 
            id="user.attributes.num-fosa--strahlentherapie" 
            onchange="apply_rules(this);"/>
      <label for="user.attributes.num-fosa--strahlentherapie">Strahlentherapie</label>
    </div>
  
    </div>
</div>


          
              <div class="form-group">
   <div class="${properties.kcInputWrapperClass!}">
    ORCID
    
      <p><a href="https://orcid.org" target="_blank" >ORCID</a> wird genutzt, um Informationen zur wissenschaftlichen Arbeit (Publikationen und Poster) der Nutzer:innen abzufragen und diese intern im NUM- Hub anzuzeigen</p>
       <input type="text"  
        class="${properties.kcInputClass!}" 
        id="user.attributes.ORCID" 
        name="user.attributes.ORCID" 
        value="${(register.formData['user.attributes.ORCID']!'')}"  
        placeholder="0123-4567-8901-2345"/>
   </div>
</div>

                  
          <div class="mt-3">
              
              <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6"
                 style="display:visible" >
                <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(-1)"><i class="fa-solid fa-left"></i>Zurück</button>
              </div>
                            
                <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(1)">Weiter <i class="fa-solid fa-right"></i></button>
                </div>                
              

          </div>
        </section>
      
        <section id="step-5" class="form-step d-none">
          <h2>Anmeldedaten</h2>
          <p><sub>*</sub> = Pflichtfeld</p>
          
              <#if !realm.registrationEmailAsUsername>
<div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">

    <div class="${properties.kcInputWrapperClass!}">${msg("username")} <sub>*</sub>
      <p>Format: vorname.nachname</p>
        <input type="text" id="username" class="${properties.kcInputClass!}" name="username" value="${(register.formData.username!'')}" autocomplete="username" />
    </div>
</div>
</#if>

          
              <#if passwordRequired??>
<div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password',properties.kcFormGroupErrorClass!)}">
  <div class="${properties.kcLabelWrapperClass!}">
    <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label> <sub>*</sub>
    <p>Mindestens 10 Zeichen, mindestens ein Groß- und Kleinbuchstabe, sowie eine Zahl.</p>
  </div>
  <div class="${properties.kcInputWrapperClass!}">
    <input type="password" id="password" class="${properties.kcInputClass!}" name="password" autocomplete="new-password"/>
  </div>
</div>
<div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password-confirm',properties.kcFormGroupErrorClass!)}">
  <div class="${properties.kcInputWrapperClass!}">${msg("passwordConfirm")} <sub>*</sub>
    <input type="password" id="password-confirm" class="${properties.kcInputClass!}" name="password-confirm" />
  </div>
</div>
</#if>

                  
          <div class="mt-3">
              
              <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6"
                 style="display:visible" >
                <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6 btn-navigate-form-step" type="button" onclick="navigateToFormStep(-1)"><i class="fa-solid fa-left"></i>Zurück</button>
              </div>
                              
                <div id="kc-form-buttons" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} col-lg-6" type="submit" value="${msg("doRegister")}"/>
                </div>
              

          </div>
        </section>
      
            <#if recaptchaRequired??>
            <div class="form-group">
                <div class="${properties.kcInputWrapperClass!}">
                    <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                </div>
            </div>
            </#if>

            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="back-to-login ${properties.kcFormOptionsWrapperClass!}">
                        <span><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
                    </div>
                </div>
            </div>
    </form>
    <script> 
      const rules = [{"id": "show-partner-organisation", "rules": [{"id": "organisation_type", "value": "Nicht-universit\u00e4re Partnereinrichtung"}], "show": {"form_item": "Organisation", "options": ["num-organisations--cottbus", "num-organisations--dkfz", "num-organisations--fraunhofer-mevis", "num-organisations--highmed", "num-organisations--hmgu", "num-organisations--hzi", "num-organisations--ths-heilbronn", "num-organisations--tmf", "num-organisations--tu-darmstadt"]}, "triggered_by": "organisation_type"}, {"id": "show-uniklinik-organisation", "rules": [{"id": "organisation_type", "value": "Uniklinik"}], "show": {"form_item": "Organisation", "options": ["num-organisations--uk-augsburg", "num-organisations--rwth-aachen", "num-organisations--uk-augsburg", "num-organisations--charite-berlin", "num-organisations--uk-owl", "num-organisations--uk-bochum", "num-organisations--uk-bonn", "num-organisations--uk-dresden", "num-organisations--uk-duesseldorf", "num-organisations--uk-erlangen", "num-organisations--uk-essen", "num-organisations--uk-frankfurt", "num-organisations--uk-freiburg", "num-organisations--uk-marburg", "num-organisations--uk-goettingen", "num-organisations--uk-greifswald", "num-organisations--uk-halle", "num-organisations--uk-hamburg-eppendorf", "num-organisations--mh-hannover", "num-organisations--uk-heidelberg", "num-organisations--uk-saarland", "num-organisations--uk-jena", "num-organisations--uk-koeln", "num-organisations--uk-leipzig", "num-organisations--uk-magdeburg", "num-organisations--uk-mainz", "num-organisations--uk-mannheim", "num-organisations--lmu-muenchen", "num-organisations--k-recht-der-isar-muenchen", "num-organisations--uk-muenster", "num-organisations--uk-oldenburg", "num-organisations--uk-regensburg", "num-organisations--uk-rostock", "num-organisations--uk-schleswig-holstein", "num-organisations--uk-tuebingen", "num-organisations--uk-ulm", "num-organisations--uk-wuerzburg"]}, "triggered_by": "organisation_type"}, {"id": "show-num-functions", "rules": [{"id": "Ober-Organisation", "value": "unikliniken"}], "show": {"form_item": "NUM-Funktion", "options": ["loks-l", "loks-m"]}, "triggered_by": "Ober-Organisation"}, {"id": "show-num-functions", "rules": [{"id": "Ober-Organisation", "value": "otherpartners"}], "show": {"form_item": "NUM-Funktion", "options": null}, "triggered_by": "Ober-Organisation"}, {"id": "show-forschungslinie-projekte", "rules": [{"id": "num-projects-project-type--forschungslinie", "value": true}], "show": {"form_item": "Forschungslinie", "options": null}, "triggered_by": "num-projects-project-type--forschungslinie"}, {"id": "show-infrastrukturlinie-projekte", "rules": [{"id": "num-projects-project-type--infrastrukturlinie", "value": true}], "show": {"form_item": "Infrastrukturlinie", "options": null}, "triggered_by": "num-projects-project-type--infrastrukturlinie"}, {"id": "show-fosa-liste", "rules": [{"id": "fosa-beteiligung", "value": "ja-fosa-beteiligung"}], "show": {"form_item": "Fachbereich", "options": null}, "triggered_by": "fosa-beteiligung"}];      
    </script>
    <script>
function set_user_name() {
    let vorname = document.getElementById("firstName").value;
    let nachname = document.getElementById("lastName").value;
    if (vorname && vorname != '' && nachname && nachname != '')
    {
      let username = vorname.toLowerCase() + '.' + nachname.toLowerCase();
      let clean_username = username.replace(/[^\w\.]/gi, '')

      console.log(clean_username);
      document.getElementById("username").value = clean_username;
    }
}

function apply_rules(question){

  // find all corresponding rules
  let triggered_rules = rules.filter(rule => ('user.attributes.' + rule.triggered_by) == question.id);

  // check the rules whether trigger should be triggered
  for(let x in triggered_rules) {
    let rule = triggered_rules[x];

    rule.is_triggered = true;
    
    for(let y in rule.rules) {
      let trigger_rule = rule.rules[y];
      let id = 'user.attributes.' + trigger_rule.id;
      let element = document.getElementById(id);
      let value = element.value;

      if (element.type == "checkbox") {
        value = element.checked;
      }

      if (value != trigger_rule.value) {
        rule.is_triggered = false;
      }
    }
  }

  // reset every not triggered rule
  let reset_triggered_rules = triggered_rules.filter(rule => !rule.is_triggered);
  for (let x in reset_triggered_rules) {
    set_questions(reset_triggered_rules[x]);
  }

  // set every triggered rule
  let set_triggered_rules = triggered_rules.filter(rule => rule.is_triggered);
  for (let x in set_triggered_rules) {
    set_questions(set_triggered_rules[x]);
  }        
}

function set_questions(rule) {
  console.log(rule.is_triggered);
  
  let show_id = 'user.attributes.' + rule.show.form_item + ".div";
  console.log(show_id);        
  document.getElementById(show_id).classList.add("invisible");

  if(rule.is_triggered) 
    document.getElementById(show_id).classList.remove("invisible");

  if(rule.show.options) {
    for (let z in rule.show.options) {
      let option = rule.show.options[z]
      let option_id = 'user.attributes.' + rule.show.form_item + "." + option;

      document.getElementById(option_id).classList.add("hidden");
      if(rule.is_triggered) 
      
        document.getElementById(option_id).classList.remove("hidden");
    }
  }
}

function set_other(field, input, html_class) {
  console.log(input.value);
  if ( input.value == 'other' ) {
    let outerHTML = "<input type='text' " + 
      ((field.required == "True") ? 'required ' : '') + 
      "class='" + html_class + "' id='user.attributes." + 
      field.attribute + "' name='user.attributes." + 
      field.attribute + "'  placeholder='"+
      field.other + "' />";
    input.outerHTML = outerHTML;
  }
  else {
    let id = '#' + input.value;
    $(id).removeClass("hidden");
  }      
}

var currentTab = 1; // Current tab is set to be the first tab (0)

function validateForm() {
  // This function deals with validation of the form fields
  var section, inputs, i, valid = true;
  section = document.getElementsByClassName("form-step");
  inputs = section[currentTab -1].querySelectorAll("[required]");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < inputs.length; i++) {
    // If a field is empty...
    if (inputs[i].value == "") {
      // add an "invalid" class to the field:
      inputs[i].classList.add("invalid")
      // and set the current valid status to false:
      valid = false;
    }
    else {
      inputs[i].classList.remove("invalid")
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementById("step-header-"+ currentTab).classList.add("finish");
  }
  return valid; // return the valid status
}

/**
 * Define a function to navigate betweens form steps.
 * It accepts one parameter. That is - step number.
 */
 const navigateToFormStep = (step) => {
    /**
     * Hide all form steps.
     */
    if (step == 1 && !validateForm ()) { 
      return false;
    }

    currentTab += step;
    
    console.log(currentTab)

    if(currentTab >1){
      var headerText = document.getElementsByClassName("alert-info-header");
      for (var i = 0; i < headerText.length; i++) {
        headerText[i].style.display='none';  
      }
      var registerCard = document.getElementsByClassName("card-pf");
      registerCard[0].style["margin-top"]='50px';  
    }
   
    document.querySelectorAll(".form-step").forEach((formStepElement) => {
        formStepElement.classList.add("d-none");
    });
    /**
     * Mark all form steps as unfinished.
     */
    document.querySelectorAll(".form-stepper-list").forEach((formStepHeader) => {
        formStepHeader.classList.add("form-stepper-unfinished");
        formStepHeader.classList.remove("form-stepper-active", "form-stepper-completed");
    });
    /**
     * Show the current form step (as passed to the function).
     */
    document.querySelector("#step-" + currentTab).classList.remove("d-none");
    /**
     * Select the form step circle (progress bar).
     */
    const formStepCircle = document.querySelector('li[step="' + currentTab + '"]');
    /**
     * Mark the current form step as active.
     */
    formStepCircle.classList.remove("form-stepper-unfinished", "form-stepper-completed");
    formStepCircle.classList.add("form-stepper-active");
    /**
     * Loop through each form step circles.
     * This loop will continue up to the current step number.
     * Example: If the current step is 3,
     * then the loop will perform operations for step 1 and 2.
     */
    for (let index = 0; index < currentTab; index++) {
        /**
         * Select the form step circle (progress bar).
         */
        const formStepCircle = document.querySelector('li[step="' + index + '"]');
        /**
         * Check if the element exist. If yes, then proceed.
         */
        if (formStepCircle) {
            /**
             * Mark the form step as completed.
             */
            formStepCircle.classList.remove("form-stepper-unfinished", "form-stepper-active");
            formStepCircle.classList.add("form-stepper-completed");
        }
    }
};</script>
  </#if>
</@layout.registrationLayout>
